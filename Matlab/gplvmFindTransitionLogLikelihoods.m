function model = gplvmFindTransitionLogLikelihoods( model, X )
%GPLVMFINDTRANSITIONLOGLIKELIHOODS Determine the log likelihood of reaching
%each sample from each other sample.
%   Generates a NxN matrix of transitions based on the maximum transition
%   likelihood given the multimodel dynamics of the model.

    if(nargin < 2)
        X = model.X;
    end
    
    N = size(X,1);
    
    % Base case: assume all transitions are infinitely small.
    overall_tll = repmat(-inf,N,N);

    for i = 1:model.dynamics.numModels
        tll = zeros(N);
        dyn = model.dynamics.comp{i};

        for a = 1:N
            for b = 1:N
                tll(a,b) = gpDynamicsPointLogLikelihood(dyn, X(a,:), X(b,:)-X(a,:));
            end
        end
        model.dynamics.comp{i}.tll = tll;
        overall_tll = max(overall_tll, tll);
    end

    model.tll = overall_tll;

end

