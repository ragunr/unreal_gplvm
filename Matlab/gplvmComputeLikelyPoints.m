function points = gplvmComputeLikelyPoints( model, resolution, varsigma_thresh, ydist_thresh )

    %resolution = 10;

    % Generate a grid of potential points covering the active region in all
    % dimensions.
    xMin = min(model.X);
    xMax = max(model.X);
    xSpan = xMax - xMin;
    xMin = xMin - 0.05*xSpan;
    xMax = xMax + 0.05*xSpan;
    
    x = cell(model.q,1);
    for i = 1:model.q
        x{i,1} = linspace(xMin(i), xMax(i), resolution);
    end
    
    XTest = cell(model.q,1);
    [XTest{:}] = ndgrid(x{:});

    % reshape our grid such that we have one sample per row
    XTest = reshape(cell2mat( cellfun(@(v)v(:), XTest, 'UniformOutput',false) ) , [], model.q);

    % determine the distance from each points to their nearest sample
    % point.
    xdists = min_distance(XTest, model.X);
    
    orig_xdists = min_distance(model.X);
    
    % Exclude any points that are nowhere near the original samples.
    XTest = XTest(xdists < max(orig_xdists),:);
    
    % Compute our poses and sigma for each test point
    [mu, varsigma] = fgplvmPosteriorMeanVar(model, XTest);
    mean_varsigma = mean(log(max(varsigma,eps)),2);
    
    % Determine the distance from previously existing poses, so we can
    % eliminate points that don't generate novel poses.
    ydists = min_distance(mu, model.y);
    
    %orig_ydists = min_distance(model.y);
    
    % elimate points that are low confidence or non-novel.
    %points = XTest(mean_varsigma < -6.1 & ydists > 3.5,:);
    points = XTest(mean_varsigma < varsigma_thresh & ydists > ydist_thresh,:);
    
end

function dists = min_distance(tests, examples)
    if nargin < 2
        examples = tests;
        self_compare = true;
    else
        self_compare = false;
    end
    N = size(tests,1);
    dists = zeros(N,1);
    for i = 1:N
        diffs = examples - repmat(tests(i,:), size(examples,1), 1);
        if self_compare
            diffs(i,:) = [];
        end
        dists(i) = min(sum(diffs.^2,2).^0.5);
    end
end
