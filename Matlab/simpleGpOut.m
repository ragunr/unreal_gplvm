function y = simpleGplvmOut(model, x)
    KX_star = simpleRbfBiasWhiteKernCompute(model, model.X, x);  
    y = KX_star'*model.alpha;
    y = y .* model.scale;
    y = y + model.bias;
end

