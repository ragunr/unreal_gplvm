function gX = simpleRbfBiasWhiteGradX(model, mX, x)
    rbfInverseWidth = model.kern_params(1);
    rbfVariance = model.kern_params(2);
    biasVariance = model.kern_params(3);
    whiteVariance = model.kern_params(4);
    
    gX = zeros(size(mX));
    n2 = dist2(mX, x);
    wi2 = (0.5 * rbfInverseWidth);
    rbfPart = rbfVariance*exp(-n2*wi2);
    for i = 1:size(x,2)
        gX(:, i) = rbfInverseWidth * (mX(:,i) - x(i)).*rbfPart;
    end
end