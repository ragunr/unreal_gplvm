function gsimgmavar = simpleGpVarGradient(model, x)
    kX = simpleRbfBiasWhiteKernCompute(model, model.X, x);  
    gX = simpleRbfBiasWhiteGradX(model, model.X, x);
    Kinvgk = model.invK_uu*gX;
    % it appears rbf and likely bias white are zeros on their gradiant diagonal
    gsimgmavar = - 2*Kinvgk'*kX;
end