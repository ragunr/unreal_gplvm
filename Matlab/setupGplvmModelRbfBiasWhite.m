function model = setupGplvmModelRbfBiasWhite( Y, q, series )
    % Sets up a model using the classic rbf/bias/white kernel.

    d = size(Y,2);
    N = size(Y,1);

    if(nargin < 3)
        series = ones(N,1);
    end
    
    options = fgplvmOptions('ftc');
    options.learnScales = false;
    options.isSpherical = true;
    options.kern = kernCreate(Y,{'rbf','bias','white'});
    options.kern.comp{1,1}.inverseWidth = 0.2;
    options.kern.comp{1,1}.variance = 1e-2;
    options.kern.comp{1,2}.variance = 1e-3;
    options.kern.comp{1,3}.variance = 1e-3;
    
    % Experiments with how initialization strategies effect the optimized
    % result.
    
    %options.initX = 'isomap';
    %options.initX = repmat([1:size(Y,1)]',1,q)*0.01+rand(size(Y,1),q)*0.01;
%     rX = (rand(size(Y,1),q)-0.5)*0.1;
%     d2 = sum(rX.^2,2);
%     [void, si] = sort(d2);
%     options.initX = rX(si,:);
%     d2n = sum(rX.^2,2);
    
    ring_initX = false;
    if(ring_initX)
        options.initX = zeros(N, q);
        for s = 1:max(series)
            sIdx = series == s;
            sN = sum(sIdx);
            options.initX(sIdx,:) = repmat(sin((1:sN)*2*pi/sN)',1,q);
            options.initX(sIdx,2:2:q) = repmat(cos((1:sN)*2*pi/sN)',1,floor(q/2));
            options.initX(sIdx,:) = options.initX(sIdx,:) +  rand(sN,q)*1e-2;
            options.initX(sIdx,1) = options.initX(sIdx,1) + ones(sN,1)*1e-1*s;
        end
    end

    model = fgplvmCreate(q, d, Y, options);

end

