function model2 = gplvmPrepImport(model)
    %GPLVMPREPIMPORT ensure that all of the expected parameters exist for
    %each model being imported.
    model = prep_model(model, 0);

    for i = 1:model.dynamics.numModels
        model.dynamics.comp{i} = prep_model(model.dynamics.comp{i}, 1);
    end

    model.reverseModel = prep_model(model.reverseModel, 0);
    model2 = model;
    save recorded/test.mat
end

function model = prep_model(model, kern_type)
    if(~isfield(model,'kern_type')) 
        model.kern_type = kern_type;
    end
    if(~isfield(model,'kern_params')) 
        model = prep_kern_params(model);
    end
    if(~isfield(model,'alpha')) 
        model = gpComputeAlpha(model);
    end
    if(~isfield(model,'m')) 
        model.m = gpComputeM(model);
    end
end


function model = prep_kern_params(model)
    switch(model.kern_type)
        case 0
            model.kern_params = [ ...
                rbfKernExtractParam(model.kern.comp{1,1})...
                biasKernExtractParam(model.kern.comp{1,2})...
                whiteKernExtractParam(model.kern.comp{1,3})];
        case 1
            model.kern_params = [...
                rbfKernExtractParam(model.kern)];
    end
end
