function model = gplvmPreprocess(Y, q, iters, display, connections, discover_poses)

    sensitivity = 1e-10;

    save recorded/start.mat
    
    % Fix seeds
    randn('seed', 1e5);
    rand('seed', 1e5);
    
    % removed fixed indexes from computation
    Y_dynamic = var(Y) >= sensitivity;
    
    Y_dynamic(end-(8*12):end) = false; % TODO: Find a better way to exclude IK joints
    
    Y_reduced = Y(:,Y_dynamic);
    
    d = size(Y_reduced, 2);
    N = size(Y_reduced, 1);
    
    % determine which poses are in each series
    series = zeros(N, 1);
    for i = 1:N
        if(series(i) == 0) series(i) = max(series)+1; end
        next = find(connections(:,1) == i);
        if(next)
            next = connections(next(1),2);
            if(next == i+1)series(next) = series(i); end
        end
    end
    
    
    % Build Model
    model = setupGplvmModelRbfBiasWhite(Y_reduced, q, series);

    varscales = var(Y_reduced);
    varscales(varscales < 1) = 1;
    model.scale = varscales;
    
%     if(iters > 0) 
%         model = fgplvmOptimise(model, display, iters);
%     end
    
    save recorded/nodynamics.mat
    
    % Add dynamics model.
    model = setupDynamicsModelMultiRbf(model,connections, series);
    
    % Optimise the model.
    % iters = 1000; % parameter
    
    
    if(iters > 0) 
        model = fgplvmOptimise(model, display, iters);
    end
    % Compute alpha and m early for fast c side synthesis
    model.m = gpComputeM(model);
    model = gpComputeAlpha(model); 
    
    model.dynamicColumns = Y_dynamic;
    model.dynamicY = Y_dynamic;
    
    reverseOptions = gpOptions('ftc');
    reverseOptions.isSpherical = true;
    reverseModel = gpCreate(d, q, Y_reduced, model.X, reverseOptions);
    if(iters > 0) 
        % currently disabled as naive approach to reverse model was
        % unsuccessful.
        reverseModel = gpOptimise(reverseModel, 1, 0);
    end
    reverseModel.kern_type = 0;
    model.reverseModel = reverseModel;
    
    %model.latentController = generateLatentController(model, iters);
    
    model = gplvmFindTransitionLogLikelihoods(model);
    
    % Save the results.
    modelWriteResult(model, 'Dynamic', display);
    
    % determine the labels before we save teh workspace.
    lbls = zeros(size(Y,1),max(series));
    for i = 1:size(Y_reduced,1)
        lbls(i,series(i)) = 1;
    end
    
    if discover_poses
        % Parameters found by experimentation.
        % Future work: Automatically find discovery parameters
        Xnew = gplvmComputeLikelyPoints(model, 10, -6.1, 3.5);
        model.expX = [model.X; Xnew];
        model.expN = model.N+size(Xnew,1);
        %lvmScatterPlot3D(model,lbls,[],1:3);
        %hold on
        %Snew = cell(1,size(Xnew,1));
        %Snew(:) = {'y*'};
        %lvmThreeDPlot(Xnew, [], Snew);
        %hold off
    else
        model.expX = model.X;
        model.expN = model.N;
    end
    
    model = gplvmFindTransitionLogLikelihoods(model, model.expX);
    
    model = gplvmPrepImport(model);
    
    save recorded/end.mat;
    
    %display = 1;
    if(display == 1)
        if(size(model.X,2) == 2) 
            lvmScatterPlot(model,lbls);
        elseif(size(model.X,2) >= 3) 
            lvmScatterPlot3D(model,lbls);
        end
    end
            
end
    