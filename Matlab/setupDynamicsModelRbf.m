function model = setupDynamicsModelRbf(model, connections, series)
    % Sets up a simple rbf dynamics model.
    [N q] = size(model.X);
    
    options = gpOptions('ftc');
    
    options.connections = connections;
    options.completeN = N;

    options.kern = kernCreate(model.X, 'rbf');  
    options.kern.inverseWidth = 0.2;
    options.kern.variance = 1e-3;

    model = fgplvmAddDynamics(model, 'gp', options,1,1);

end
