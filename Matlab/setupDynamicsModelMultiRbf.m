function model = setupDynamicsModelMultiRbf(model, connections, series)
    % Sets up a dynamics multi model using multiple rbf models.
    [N q] = size(model.X);
    SN = max(series);
    
    if(nargin < 3)
        series = ones(N,1);
    end

    multioptions = multimodelOptions('gpDynamics', SN, 'ftc');
    multioptions.separate = 1:2;
    multioptions.compOptions = cell(1,SN);
    for s = 1:max(series)
        options = gpOptions('ftc');
        
        sIdx = ismember(connections(:,2), find(series == s));
        options.connections = connections(sIdx, :);
        options.completeN = N;
        
        options.kern = kernCreate(model.X, 'rbf');  
        options.kern.inverseWidth = 0.2;
        options.kern.variance = 1e-3;
        
        multioptions.compOptions{s} = options;
    end
    
    model.dynamics = modelCreate('multimodel', ...
            model.q, ...
            model.q, ...
            repmat({model.X}, 1, SN), ...
            repmat({1}, 1, SN), ...
            repmat({1}, 1, SN), ...
            multioptions);
    
    % Because I can't use gpAddDynamics with the multimodel dynamics I have
    % to manually perform the extract / expand
    params = fgplvmExtractParam(model);
    model.dynamics.numParams = length(modelExtractParam(model.dynamics));
    model.numParams = model.numParams + model.dynamics.numParams;
    model = fgplvmExpandParam(model, params);


end
