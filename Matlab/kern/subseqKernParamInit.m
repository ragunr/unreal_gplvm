function kern = subseqKernParamInit(kern)

% SUBSEQKERNPARAMINIT SUBSEQ kernel parameter initialisation.
% The subsequence covariance function returns a nested kernel should two
% indexes be from the same subsequence and zero otherwise.
%
% FORMAT
% DESC initialises the index based covariance function
%  kernel structure with some default parameters.
% ARG kern : the kernel structure which requires initialisation.
% ARG subkern : the kernel to use when in the same subsequence.
% RETURN kern : the kernel structure with the default parameters placed in.
%
% SEEALSO : kernCreate, kernParamInit
%
% COPYRIGHT : Richard Lester, 2017

% KERN

% These parameters are restricted to lie between 0 and 1.
  kern.nParams = 0;
  kern.paramGroups = speye(kern.nParams);
  kern.subkern = [];
  kern.series = [];
  
  %kern.transforms(1).index = [1];
  %kern.transforms(1).type = optimiDefaultConstraint('positive');

  kern.isStationary = true;
end
