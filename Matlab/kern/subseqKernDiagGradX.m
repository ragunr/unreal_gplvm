function gX = subseqKernDiagGradX(kern, X)


% SUBSEQKERNDIAGGRADX Gradient of SUBSEQ kernel's diagonal with respect to X.
% FORMAT
% DESC computes the gradient of the diagonal of the subsequence kernel matrix with
% respect to the elements of the design matrix given in X.
% ARG kern : the kernel structure for which gradients are being computed.
% ARG X : the input data in the form of a design matrix.
% RETURN gX : the gradients of the diagonal with respect to each element
% of X. The returned matrix has the same dimensions as X.
%
% SEEALSO : subseqKernParamInit, kernDiagGradX, subseqkernGradX
%
% COPYRIGHT : Neil D. Lawrence, 2004, 2005, 2006, Richard Lester 2017

% KERN


series_enum = unique(kern.series);
gX = zeros(size(X));

for i = 2:series_enum
    
    in_series = kern.series == i;
    % only part of the data is involved with the kernel.
    gX(in_series,:) = ...
        gX(in_series,:) + ...
        kernDiagGradX(kern.subkern, ...
                  X(in_series,:));

end
