function k = subseqKernCompute(kern, varargin)

% INDEXKERNCOMPUTE Compute the INDEX kernel given the parameters and X.
% FORMAT
% DESC computes the kernel parameters for the index based covariance function
% kernel given inputs associated with rows and columns.
% ARG kern : the kernel structure for which the matrix is computed.
% ARG x : the input matrix associated with the rows of the kernel.
% ARG x2 : the input matrix associated with the columns of the kernel.
% RETURN k : the kernel matrix computed at the given points.
%
% FORMAT
% DESC computes the kernel matrix for the index based covariance function
% kernel given a design matrix of inputs.
% ARG kern : the kernel structure for which the matrix is computed.
% ARG x : input data matrix in the form of a design matrix.
% RETURN k : the kernel matrix computed at the given points.
%
% SEEALSO : subseqKernParamInit, kernCompute, kernCreate, subseqKernDiagCompute
%
% COPYRIGHT : Neil D. Lawrence, 2011  Richard Lester, 2017

% KERN

%   i1 = x(:,end);
% 
%   if nargin<3
%     x2 = x;
%   end
%   i2 = x2(:,end);

  series_ip = sqrt(kern.series'*kern.series);
  same_series = floor(series_ip) == series_ip;
  k = kernCompute(kern.subkern, varargin{:});
  k(~same_series) = 0;
end
