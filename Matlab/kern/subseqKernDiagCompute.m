function k = subseqKernDiagCompute(kern, x)

% SUBSEQKERNDIAGCOMPUTE Compute diagonal of SUBSEQ kernel.
% FORMAT
% DESC computes the diagonal of the kernel matrix for the index based covariance function kernel given a design matrix of inputs.
% ARG kern : the kernel structure for which the matrix is computed.
% ARG x : input data matrix in the form of a design matrix.
% RETURN k : a vector containing the diagonal of the kernel matrix
% computed at the given points.
%
% SEEALSO : subseqKernParamInit, kernDiagCompute, kernCreate, subseqKernCompute
%
% COPYRIGHT : Neil D. Lawrence, 2011, Ricahrd Lester, 2017

% KERN
  k = kernDiagCompute(kern.subkern,x);
end
