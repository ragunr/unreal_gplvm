﻿// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System;
using System.IO;



public class MatlabEngine : ModuleRules
{
	public MatlabEngine(TargetInfo Target)
	{
		Type = ModuleType.External;

        // Determine the root directory of Matlab
        string MatlabBaseDir = Path.Combine(ModuleDirectory);

		// Add the libraries for the current platform

		//string WindowsVersion = "vs" + WindowsPlatform.GetVisualStudioCompilerVersionName();
		string ArchitectureVersion = (Target.Platform == UnrealTargetPlatform.Win64) ? "win64" : "win32";
		//string ConfigVersion = (Target.Configuration == UnrealTargetConfiguration.Debug && BuildConfiguration.bDebugBuildsActuallyUseDebugCRT) ? "Debug" : "Release";

        string MatlabLibDir = Path.Combine(MatlabBaseDir, "Libraries", ArchitectureVersion);
		PublicLibraryPaths.Add(MatlabLibDir);

		PublicAdditionalLibraries.Add("libmx.lib");
		PublicAdditionalLibraries.Add("libeng.lib");

        string MatlabIncDir = Path.Combine(MatlabBaseDir, "Include");
		PublicSystemIncludePaths.Add(MatlabIncDir);
	}
}

