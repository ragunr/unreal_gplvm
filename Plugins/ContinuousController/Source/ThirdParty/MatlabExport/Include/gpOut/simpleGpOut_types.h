//
// File: simpleGpOut_types.h
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 21-Feb-2017 13:00:09
//
#ifndef __SIMPLEGPOUT_TYPES_H__
#define __SIMPLEGPOUT_TYPES_H__

// Include Files
#include "rtwtypes.h"

// Type Definitions
#ifndef struct_emxArray__common
#define struct_emxArray__common

struct emxArray__common
{
  void *data;
  int *size;
  int allocatedSize;
  int numDimensions;
  boolean_T canFreeData;
};

#endif                                 //struct_emxArray__common

#ifndef struct_emxArray_boolean_T
#define struct_emxArray_boolean_T

struct emxArray_boolean_T
{
  boolean_T *data;
  int *size;
  int allocatedSize;
  int numDimensions;
  boolean_T canFreeData;
};

#endif                                 //struct_emxArray_boolean_T

#ifndef struct_emxArray_int32_T
#define struct_emxArray_int32_T

struct emxArray_int32_T
{
  int *data;
  int *size;
  int allocatedSize;
  int numDimensions;
  boolean_T canFreeData;
};

#endif                                 //struct_emxArray_int32_T

#ifndef struct_emxArray_real32_T
#define struct_emxArray_real32_T

struct emxArray_real32_T
{
  float *data;
  int *size;
  int allocatedSize;
  int numDimensions;
  boolean_T canFreeData;
};

#endif                                 //struct_emxArray_real32_T

typedef struct {
  emxArray_real32_T *kern_params;
  float d;
  float N;
  float q;
  emxArray_real32_T *X;
  emxArray_real32_T *scale;
  emxArray_real32_T *bias;
  emxArray_real32_T *alpha;
} struct0_T;

#endif

//
// File trailer for simpleGpOut_types.h
//
// [EOF]
//
