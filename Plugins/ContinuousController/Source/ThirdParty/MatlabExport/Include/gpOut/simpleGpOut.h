//
// File: simpleGpOut.h
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 26-Feb-2017 23:50:34
//
#ifndef __SIMPLEGPOUT_H__
#define __SIMPLEGPOUT_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rtwtypes.h"
#include "simpleGp_types.h"

// Function Declarations
extern void simpleGpOut(const struct0_T *model, const emxArray_real32_T *x,
  emxArray_real32_T *y);

#endif

//
// File trailer for simpleGpOut.h
//
// [EOF]
//
