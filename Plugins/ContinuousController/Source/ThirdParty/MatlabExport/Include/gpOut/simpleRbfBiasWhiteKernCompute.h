//
// File: simpleRbfBiasWhiteKernCompute.h
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 26-Feb-2017 23:50:34
//
#ifndef __SIMPLERBFBIASWHITEKERNCOMPUTE_H__
#define __SIMPLERBFBIASWHITEKERNCOMPUTE_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rtwtypes.h"
#include "simpleGp_types.h"

// Function Declarations
extern void simpleRbfBiasWhiteKernCompute(const emxArray_real32_T
  *model_kern_params, const emxArray_real32_T *x, const emxArray_real32_T *x2,
  emxArray_real32_T *k);

#endif

//
// File trailer for simpleRbfBiasWhiteKernCompute.h
//
// [EOF]
//
