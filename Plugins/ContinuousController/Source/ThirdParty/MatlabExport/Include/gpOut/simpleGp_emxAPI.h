//
// File: simpleGp_emxAPI.h
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 26-Feb-2017 23:50:34
//
#ifndef __SIMPLEGP_EMXAPI_H__
#define __SIMPLEGP_EMXAPI_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rtwtypes.h"
#include "simpleGp_types.h"

// Function Declarations
extern emxArray_real32_T *emxCreateND_real32_T(int numDimensions, int *size);
extern emxArray_real32_T *emxCreateWrapperND_real32_T(float *data, int
  numDimensions, int *size);
extern emxArray_real32_T *emxCreateWrapper_real32_T(float *data, int rows, int
  cols);
extern emxArray_real32_T *emxCreate_real32_T(int rows, int cols);
extern void emxDestroyArray_real32_T(emxArray_real32_T *emxArray);
extern void emxDestroy_struct0_T(struct0_T emxArray);
extern void emxInitArray_real32_T(emxArray_real32_T **pEmxArray, int
  numDimensions);
extern void emxInit_struct0_T(struct0_T *pStruct);

#endif

//
// File trailer for simpleGp_emxAPI.h
//
// [EOF]
//
