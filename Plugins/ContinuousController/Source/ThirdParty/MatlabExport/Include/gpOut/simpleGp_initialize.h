//
// File: simpleGp_initialize.h
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 26-Feb-2017 23:50:34
//
#ifndef __SIMPLEGP_INITIALIZE_H__
#define __SIMPLEGP_INITIALIZE_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rtwtypes.h"
#include "simpleGp_types.h"

// Function Declarations
extern void simpleGp_initialize();

#endif

//
// File trailer for simpleGp_initialize.h
//
// [EOF]
//
