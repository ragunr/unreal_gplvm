//
// File: dist2.h
//
// MATLAB Coder version            : 2.8
// C/C++ source code generated on  : 26-Feb-2017 23:50:34
//
#ifndef __DIST2_H__
#define __DIST2_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rtwtypes.h"
#include "simpleGp_types.h"

// Function Declarations
extern void dist2(const emxArray_real32_T *x, const emxArray_real32_T *c,
                  emxArray_real32_T *n2);

#endif

//
// File trailer for dist2.h
//
// [EOF]
//
