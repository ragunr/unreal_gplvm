// Some copyright should be here...

using UnrealBuildTool;

public class ContinuousControllerRuntime : ModuleRules
{
	public ContinuousControllerRuntime(TargetInfo Target)
	{
		
		PublicIncludePaths.AddRange(
			new string[] {
				"ContinuousControllerRuntime/Public"
				
				// ... add public include paths required here ...
			}
			);
				
		
		PrivateIncludePaths.AddRange(
			new string[] {
				"ContinuousControllerRuntime/Private",
				
				// ... add other private include paths required here ...
			}
			);
			
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core", "CoreUObject", "Engine",
                "MatlabExport", // Experimental
			}
			);
			
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				// ... add private dependencies that you statically link with here ...	
                "MatlabExport", // Experimental
			}
			);
		
		
		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
				
				// ... add any modules that your module loads dynamically here ...
			}
			);
	}
}
