#include "ContinuousControllerRuntimePrivatePCH.h"
#include "ExponentialMap.h"

#define NORM_TOLERANCE 0.00001

FExponentialMap::FExponentialMap()
{
	theta = 1;
	w = FVector::ZeroVector;
}

FExponentialMap::FExponentialMap(FQuat & _Quat)
{
	FQuat nQuat = _Quat.GetNormalized(NORM_TOLERANCE);
	nQuat.ToAxisAndAngle(w, theta);
}

FQuat FExponentialMap::getQuat()
{
	return FQuat(w, theta);
}
