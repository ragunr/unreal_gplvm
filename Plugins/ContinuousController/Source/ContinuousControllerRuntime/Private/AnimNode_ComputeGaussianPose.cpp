
#include "ContinuousControllerRuntimePrivatePCH.h"
#include "AnimNode_ComputeGaussianPose.h"

FAnimNode_ComputeGaussianPose::FAnimNode_ComputeGaussianPose(void)
	: FAnimNode_Base()
{

}

void FAnimNode_ComputeGaussianPose::Initialize(const FAnimationInitializeContext& Context)
{
	//ComponentPose.Initialize(Context);
}

void FAnimNode_ComputeGaussianPose::Update(const FAnimationUpdateContext& Context)
{
	EvaluateGraphExposedInputs.Execute(Context);
	//ComponentPose.Update(Context);
}

void FAnimNode_ComputeGaussianPose::CacheBones(const FAnimationCacheBonesContext & Context)
{
	//InitializeBoneReferences(Context.AnimInstance->RequiredBones);
	//ComponentPose.CacheBones(Context);
}

void FAnimNode_ComputeGaussianPose::Evaluate(FPoseContext& Output)
{

	Output.Pose.ResetToRefPose();
	USkeleton* Skeleton = Controller->GetSkeleton();
	if ( !Skeleton || !Controller )
	{
		return;
	}
	//return;


	TArray<float> PoseData;
	Controller->ComputePoseAtLatentPoint(LatentPoint, PoseData);

	//Controller->BuildPose(PoseData, Output.Pose);

	if (PoseData.Num() < Output.Pose.GetNumBones() * GPLVM_VALUES_PER_BONE)
	{
		UE_LOG(LogTemp, Log, TEXT("Too few data points to build pose!"));
		return;
	}

	//For testing refpose
	//int d = PoseData.Num();
	//PoseData.Reset();
	//PoseData.SetNumZeroed(d);


	// Accumulate transforms in an array for conversion to pose
	TArray<FTransform> boneTransforms;
	//boneTransforms.AddDefaulted(Output.Pose.GetNumBones());
	Output.Pose.CopyBonesTo(boneTransforms);

	int data_i = 0;
	for (auto BoneIndex : Output.Pose.ForEachBoneIndex())
	{
		//UE_LOG(LogTemp, Warning, TEXT("found bone %d."),  BoneIndex.GetInt());

		// copy the bone data for all the required bones
		volatile int bone_i = BoneIndex.GetInt();
		FTransform pose_transform;

		FVector trans_delta = FVector(
			PoseData[data_i * GPLVM_VALUES_PER_BONE + 3],
			PoseData[data_i * GPLVM_VALUES_PER_BONE + 4],
			PoseData[data_i * GPLVM_VALUES_PER_BONE + 5]);
		boneTransforms[bone_i].AddToTranslation(trans_delta);

		FVector expmap = FVector(
			PoseData[data_i * GPLVM_VALUES_PER_BONE + 0],
			PoseData[data_i * GPLVM_VALUES_PER_BONE + 1],
			PoseData[data_i * GPLVM_VALUES_PER_BONE + 2]);
		float theta = expmap.Size();
		FQuat delta = FQuat(expmap / theta, theta);
		if (theta == 0) delta = FQuat::Identity;
		boneTransforms[bone_i].ConcatenateRotation(delta);


		data_i += 1;
	}
	// apply transformations to the pose
	Output.Pose.CopyBonesFrom(boneTransforms);
	//UE_LOG(LogTemp, Log, TEXT("Evaluate complete"));
	return;
}