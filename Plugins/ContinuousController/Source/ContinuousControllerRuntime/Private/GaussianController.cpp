// Fill out your copyright notice in the Description page of Project Settings.

#include "ContinuousControllerRuntimePrivatePCH.h"
#include "GaussianController.h"
#include "IGaussianPrecomputer.h"
#include "gpOut/simpleGpOut.h"
#include "gpOut/simpleGp_emxAPI.h"
#include "gpOut/simpleGp_emxutil.h"
#include "gpOut/simpleGpVarGradient.h"

// latent variable falloff rate. 1 means loses all momentum over sample rate, 
// 0 means never loses momentum
#define GPLVM_LATENT_VEL_FALLOFF_RATE 1

UGaussianController::UGaussianController(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

void UGaussianController::ExtractPoses(TArray<FCompactPose>& PoseSampleData, FCompactPose& ReferencePose)
{
	// Collect pose samples while we still have access to our skeleton.
	// generate a pose using our skeleton so we can have the unreal code 
	// do the heavy lifting of pose construction from animation.
	FCompactPose base_pose;
	FBlendedCurve curve;
	FBoneContainer bonecontainer;
	TArray<FBoneIndexType> requiredbones;
	for (int i = 0; i < Skeleton->GetBoneTree().Num(); i++)
	{
		requiredbones.Add(FBoneIndexType(i));
	}
	bonecontainer.InitializeTo(requiredbones, *Skeleton);
	base_pose.SetBoneContainer(&bonecontainer);

	// Build our reference pose so we cand find transformations relative to that pose.
	// This helps avoid gimble lock issues as physical joints never reach 180 degrees.
	FCompactPose ref_pose(base_pose);
	SampleData[0].Animation->GetAnimationPose(ref_pose, curve, FAnimExtractContext(0, true));
	ref_pose.ResetToRefPose();
	ReferencePose = ref_pose;

	TMap<FString, int32> startframes;
	TMap<FString, int32> endframes;
	// Go through each sample and generate a pose for each frame of each animation
	int frame_i = 0;
	int next_frame = frame_i + 1;
	for (auto sample : SampleData)
	{
		TAssetPtr<UAnimSequence> animation = sample.Animation;
		animation.LoadSynchronous();

		int start_frame = frame_i;

		startframes.Add(sample.TaskName, start_frame);

		float time = 0;
		while (true)
		{
			FCompactPose pose(base_pose);
			next_frame = frame_i + 1;

			if (SampleRate > 0.0)
			{
				if (time + SampleRate / 2 >= animation->GetPlayLength()) break;

				if (time + SampleRate * 3 / 2 >= animation->GetPlayLength())
					next_frame = (sample.Looping ? start_frame : frame_i);
			}
			else
			{
				if (frame_i - start_frame >= animation->GetNumberOfFrames()) break;

				time = animation->GetTimeAtFrame(frame_i-start_frame);
		
				if(frame_i - start_frame + 1 > animation->GetNumberOfFrames())
					next_frame = (sample.Looping ? start_frame : frame_i);
				
			}

			// heavy lifting is done by GetAnimationPose
			animation->GetAnimationPose(pose, curve, FAnimExtractContext(time, true));
			PoseSampleData.Add(pose);

			TaskConnectivityMap.Find(sample.TaskName)->Map.Add(frame_i, next_frame);
			if (next_frame != frame_i)
			{
				ConnectedPairs.Add(frame_i);
				ConnectedPairs.Add(next_frame);
			}
			
			time += SampleRate;
			frame_i += 1;
		}

		endframes.Add(sample.TaskName, frame_i-1);
	}
	for (auto sample : SampleData)
	{
		if (sample.PreviousTaskName.Len() > 0)
		{
			ConnectedPairs.Add(*endframes.Find(sample.PreviousTaskName));
			ConnectedPairs.Add(*startframes.Find(sample.TaskName));
		}
		if (sample.NextTaskName.Len() > 0)
		{
			ConnectedPairs.Add(*endframes.Find(sample.TaskName));
			ConnectedPairs.Add(*startframes.Find(sample.NextTaskName));
		}
	}
}

void UGaussianController::ResetTaskConnectivityMap()
{
	TaskConnectivityMap.Empty();
	// Build empty connectivity maps
	for (auto Itr(SampleData.CreateIterator()); Itr; Itr++) {
		Itr->Animation.LoadSynchronous();
		if (TaskConnectivityMap.Contains(Itr->TaskName))
			TaskConnectivityMap.Find(Itr->TaskName)->Map.Empty(Itr->Animation->GetNumberOfFrames());
		else
			TaskConnectivityMap.Add(Itr->TaskName, *(new FGaussianConnectivityMap()));
	}
	ConnectedPairs.Empty();

}

void UGaussianController::SerializePoseData(const TArray<FCompactPose>& PoseSampleData, const FCompactPose& ReferencePose, TArray<double>& data)
{
	int num_bones = PoseSampleData[0].GetNumBones();
	int N = PoseSampleData.Num();
	data.Reset();
	data.SetNum(PoseSampleData.Num() * num_bones * GPLVM_VALUES_PER_BONE);
	
	TArray<FTransform> refTransforms;
	FCompactPose RefPose = ReferencePose; // Must use copy constructor here as CopyBonesTo is not const
	RefPose.CopyBonesTo(refTransforms);


	for (int pose_i = 0; pose_i < N; pose_i++)
	{
		TArray<FTransform> boneTransforms;
		FCompactPose Pose = PoseSampleData[pose_i]; // Must use copy constructor here as CopyBonesTo is not const
		Pose.CopyBonesTo(boneTransforms);

		for (int bone_i = 0; bone_i < num_bones; bone_i++)
		{
			FTransform transform = boneTransforms[bone_i];
			FTransform ref_transform = refTransforms[bone_i];
			
			FVector delta_trans = transform.GetTranslation()
				- ref_transform.GetTranslation();

			FQuat ref_rot = ref_transform.GetRotation();
			FQuat rot = transform.GetRotation();
			rot.EnforceShortestArcWith(ref_rot);
			FQuat delta_rot = ref_rot.Inverse() * rot;

			FVector axis;
			float angle;
			delta_rot.ToAxisAndAngle(axis, angle);
			data[(bone_i * GPLVM_VALUES_PER_BONE + 0) * N + pose_i] = axis.X*angle;
			data[(bone_i * GPLVM_VALUES_PER_BONE + 1) * N + pose_i] = axis.Y*angle;
			data[(bone_i * GPLVM_VALUES_PER_BONE + 2) * N + pose_i] = axis.Z*angle;
			data[(bone_i * GPLVM_VALUES_PER_BONE + 3) * N + pose_i] = delta_trans.X;
			data[(bone_i * GPLVM_VALUES_PER_BONE + 4) * N + pose_i] = delta_trans.Y;
			data[(bone_i * GPLVM_VALUES_PER_BONE + 5) * N + pose_i] = delta_trans.Z;
		}
	}
	// Second pass to find velocity data
	FGaussianConnectivityMap overall_map;
	for (auto entry : TaskConnectivityMap)
	{
		overall_map.Map.Append(entry.Value.Map);
	}
	for (int pose_i = 0; pose_i < N; pose_i++)
	{
		//int next_pose_i = ConnectedPairs[pose_i*2+1];
		//int prev_pose_i = (next_pose_i == pose_i ? pose_i - 1 : pose_i);
		int next_pose_i = *overall_map.Map.Find(pose_i);
		int prev_pose_i = *overall_map.Map.FindKey(next_pose_i);
		for (int bone_i = 0; bone_i < num_bones; bone_i++)
		{
			for (int j = 0; j < 6; j++)
			{
				data[(bone_i * GPLVM_VALUES_PER_BONE + 6 + j) * N + pose_i] =
					data[(bone_i * GPLVM_VALUES_PER_BONE + j) * N + next_pose_i] -
					data[(bone_i * GPLVM_VALUES_PER_BONE + j) * N + prev_pose_i];
			}
		}
	}
}

void UGaussianController::PopulateTaskIndicies()
{
	TaskIndicies.Reset();
	
	for (int i = 0; i < SampleData.Num(); i++)
	{
		TaskIndicies.Add(SampleData[i].TaskName, i);
	}
}

void UGaussianController::ComputeModel(IGaussianPrecomputer* Precomputer)
{
	ResetTaskConnectivityMap();
	PopulateTaskIndicies();

	TArray<FCompactPose> PoseSampleData;
	FCompactPose ReferencePose;
	ExtractPoses(PoseSampleData, ReferencePose);

	TArray<double> data;
	SerializePoseData(PoseSampleData, ReferencePose, data);

	// Call the precomputer to actually populate our model.
 	Precomputer->BuildModel(data, PoseSampleData.Num(), ConnectedPairs, Model, DynamicsModels, ReverseModel, TransitionLogLikelihoods);

	DynamicsModel = DynamicsModels[0];

	ComputeLatentPolicy();
}




void UGaussianController::ComputePoseAtLatentPoint(const FLatentPoint& LatentPoint, TArray<float>& Pose)
{
	if (LatentPoint.PrevSample == -1) return;

	TArray<float> Coordinates(LatentPoint.Coordinates);
	if (LatentPoint.Flock.Num() == FlockingLNBoids*Model.q) // Model.q*FGenericPlatformMath::Pow(3, Model.q))
	{
		memcpy(Coordinates.GetData(), LatentPoint.Flock.GetData(), sizeof(float)*Model.q);
	}
	Model.ComputeOutputAtLatentPoint(Coordinates, Pose);
}

void UGaussianController::ComputeLatentPointAtPose(const TArray<float> Pose, FLatentPoint & LatentPoint)
{
	TArray<float> PartialPose;
	PartialPose.Empty(ReverseModel.q);
	for (int i = 0; i < Model.dynamicY.Num(); i++)
	{
		if (Model.dynamicY[i]) PartialPose.Add(Pose[i]);
	}
	ReverseModel.ComputeOutputAtLatentPoint(PartialPose, LatentPoint.Coordinates);
	LatentPoint.PrevSample = Model.NearestLatentSample(LatentPoint.Coordinates);
}

//void UGaussianController::ComputeLatentPointAtPoseLink(const FPoseLink Pose, FLatentPoint & LatentPoint)
//{
//}

void UGaussianController::ComputeLatentPointGradient(const FLatentPoint& LatentPoint, TArray<float>& Gradient)
{
	DynamicsModel.ComputeOutputAtLatentPoint(LatentPoint.Coordinates, Gradient);

}


void UGaussianController::ComputeNextLatentPoint(const FString& taskname, const FLatentPoint& LatentPoint, float time_delta, FLatentPoint& NextLatentPoint)
{
	if (!TaskIndicies.Contains(taskname))
	{
		NextLatentPoint = LatentPoint;
		return;
	}
	FLatentPoint PLatentPoint = LatentPoint;
	// If our point is invalid in some way, initialize it.
	if (PLatentPoint.Coordinates.Num() != Model.q ||
		PLatentPoint.PrevSample == -1 ||
		PLatentPoint.PrevSample >= Model.extendedN)
	{
		Model.getX(0, PLatentPoint.Coordinates);
		PLatentPoint.PrevSample = 0;
	}
	switch (LatentNavigationPolicy)
	{
	case ELatentNavigationPolicy::Linear:
		ComputeNextLatentPointLinear(taskname, PLatentPoint, time_delta, NextLatentPoint);
		break;
	case ELatentNavigationPolicy::Dynamics:
		ComputeNextLatentPointDynamics(taskname, PLatentPoint, time_delta, NextLatentPoint);
		break;
	case ELatentNavigationPolicy::OptimalLikelihood:
		ComputeOptimalLikelihoodLatentPolicy(taskname, PLatentPoint.PrevSample);
		auto address = TaskIndicies[taskname] * Model.extendedN + PLatentPoint.PrevSample; // Fine, I'll hash my own damn address
		ComputeNextLatentPointLinear(OptimalLikelihoodPolicyMap[address], PLatentPoint, time_delta, NextLatentPoint);
		break;
	//case ELatentNavigationPolicy::Flocking:
		//break;
	}
	if(UseLatentFlocking)
	{
		ComputeLatentPointFlocking(PLatentPoint, time_delta, NextLatentPoint);
	}
}

void UGaussianController::ComputeNextLatentPointDynamics(const FString& taskname, const FLatentPoint& LatentPoint, float time_delta, FLatentPoint& NextLatentPoint)
{
	TArray<float> Delta;
	DynamicsModel.ComputeOutputAtLatentPoint(LatentPoint.Coordinates, Delta);
	FTArrayMath::Mult(Delta, time_delta / SampleRate, Delta);

	if(DynamicsLNWhite != 0)
		for (int i = 0; i < Delta.Num(); i++)
		{
			// Terrible gaussian sampling algorithm
			float norm_rnd = 0;
			for (int j = 0; j < 10; j++) norm_rnd += FGenericPlatformMath::SRand();
			Delta[i] += DynamicsLNWhite*norm_rnd/10*time_delta / SampleRate;
		}

	FTArrayMath::Add(LatentPoint.Coordinates, Delta, NextLatentPoint.Coordinates);

	if (DynamicsLNGrad != 0)
	{
		TArray<float> Gradient;
		Model.ComputeVarGradAtLatentPoint(NextLatentPoint.Coordinates, Gradient);
		FTArrayMath::Mult(Gradient, -DynamicsLNGrad * time_delta / SampleRate, Gradient);
		FTArrayMath::Add(NextLatentPoint.Coordinates, Gradient, NextLatentPoint.Coordinates);
	}

	NextLatentPoint.PrevSample = LatentPoint.PrevSample;
}


void UGaussianController::ComputeNextLatentPointLinear(const FString& taskname, const FLatentPoint& LatentPoint, float time_delta, FLatentPoint& NextLatentPoint)
{
	const auto& Map = (PolicyMap.Num() > 0 ? PolicyMap : TaskConnectivityMap);
	// Make sure we have a task, otherwise do nothing.
	if (taskname.Len() == 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("No task name provided"));
		NextLatentPoint = LatentPoint;
		return;
	}

	if (!Map.Contains(taskname))
	{
		UE_LOG(LogTemp, Warning, TEXT("No such task %s"), *taskname);
		NextLatentPoint = LatentPoint;
		return;
	}

	if (!Map[taskname].Map.Contains(LatentPoint.PrevSample))
	{
		UE_LOG(LogTemp, Warning, TEXT("Task %s has no next node for %i"), *taskname, LatentPoint.PrevSample);
		NextLatentPoint = LatentPoint;
		return;
	}
	ComputeNextLatentPointLinear(Map[taskname], LatentPoint, time_delta, NextLatentPoint);
}

void UGaussianController::ComputeNextLatentPointLinear(const FGaussianConnectivityMap& Map, const FLatentPoint& LatentPoint, float time_delta, FLatentPoint& NextLatentPoint)
{

	if (Model.q == 0) return;

	TArray<double> PointD;
	FTArrayMath::ToDoubles(LatentPoint.Coordinates, PointD);
	int prevSample = LatentPoint.PrevSample;


	//fix point D if it is an incorrect size.
	if (PointD.Num() != Model.q) PointD.SetNumZeroed(Model.q);

	// Run until we consume our timedelta at the end of the loop
	while (time_delta > 0)
	{
		int nextSample = Map.Map[prevSample];

		// We wish to move between point A and B. We are at point C, and after time_delta will be at point D

		TArray<double> PointA;
		Model.getX(prevSample, PointA);

		TArray<double> PointB;
		Model.getX(nextSample, PointB);

		TArray<double> PointC;
		FTArrayMath::Mult(PointD, 1.0, PointC);

		TArray<double> DeltaAB;
		FTArrayMath::Subtract(PointB, PointA, DeltaAB);

		double distAB = FTArrayMath::Distance(PointB, PointA);
		double distAC = FTArrayMath::Distance(PointC, PointA);

		double alphaC = distAC / distAB;

		double alphaStep = time_delta / SampleRate;

		// If we are going to overshoot, figure out how much timedelta it will take and snap us there.
		if (alphaC + alphaStep > 1)
		{
			//UE_LOG(LogTemp, Warning, TEXT("Snap!"));
			FTArrayMath::Mult(PointB, 1, PointD);
			prevSample = nextSample;
			time_delta -= (1 - alphaC)*SampleRate;
			continue;
		}

		double alphaD = alphaC + alphaStep;


		// Determine new location
		FTArrayMath::Lerp(PointA, PointB, alphaD, PointD);

		break;
	}
	FTArrayMath::ToFloats(PointD, NextLatentPoint.Coordinates);
	NextLatentPoint.PrevSample = prevSample;
}

void UGaussianController::ComputeLatentPointFlocking(const FLatentPoint& LatentPoint, float time_delta, FLatentPoint& NextLatentPoint)
{
	int flockN = FlockingLNBoids;//FGenericPlatformMath::Pow(3, Model.q);

	TArray<TArray<float>> Flock;
	Flock.SetNum(flockN);

	TArray<float> Target = NextLatentPoint.Coordinates;

	// Expand into nested lists to make manipulating each boyd easier.
	for (int i = 0; i < Flock.Num(); i++)
	{
		Flock[i].SetNum(Model.q);
		for (int j = 0; j < Model.q; j++)
		{
			if(LatentPoint.Flock.Num() == flockN*Model.q)
			{
				// Copy old flock
				Flock[i][j] = LatentPoint.Flock[i*Model.q + j];
			}
			else
			{
				// Initialize flock at random
				Flock[i][j] = Target[i%Target.Num()] + (-1 + 2 * FGenericPlatformMath::SRand())*FlockingLNSpread;
			}
		}
	}

	// update each flock point

	// Momentum
	// Flock follows the same trajectory as the latent point
	TArray<float> TargetDelta;
	FTArrayMath::Subtract(Target, LatentPoint.Coordinates, TargetDelta);
	FTArrayMath::Mult(TargetDelta, FlockingLNMomentum, TargetDelta);
	for (int i = 0; i < flockN; i++)
	{
		FTArrayMath::Add(Flock[i], TargetDelta, Flock[i]);
	}

	// Collapse
	// Flock collapses towards the latent point
	for (int i = 0; i < flockN; i++)
	{
		TArray<float> BoydDelta;
		FTArrayMath::Subtract(Target, Flock[i], BoydDelta);
		FTArrayMath::Mult(BoydDelta, FlockingLNCollapse*time_delta/SampleRate, BoydDelta);
		FTArrayMath::Add(Flock[i], BoydDelta, Flock[i]);
	}

	// Spread
	// Flock spreads frome one another
	TArray<TArray<float>> CopiedFlock = Flock;
	for (int i = 0; i < flockN; i++)
	{
		for (int j = 0; j < flockN; j++)
		{
			if (i == j) continue; // Dont repell from yourself.
			TArray<float> Delta;
			FTArrayMath::Subtract(Flock[i], CopiedFlock[j], Delta);
			float dist2 = FTArrayMath::LengthSquared(Delta);
			if (dist2 < 0.001) dist2 = 0.001;
			FTArrayMath::Normalize(Delta, Delta);
			FTArrayMath::Mult(Delta, 1.0/(dist2/ FlockingLNSpread), Delta);
			FTArrayMath::Mult(Delta, time_delta / SampleRate, Delta);
			FTArrayMath::Add(Flock[i], Delta, Flock[i]);
		}
	}

	// Grad
	// Flock forms to the gradiant
	for (int i = 0; i < flockN; i++)
	{
		TArray<float> Grad;
		Model.ComputeVarGradAtLatentPoint(Flock[i], Grad);
		FTArrayMath::Mult(Grad, -FlockingLNGrad*time_delta / SampleRate, Grad);
		FTArrayMath::Add(Flock[i], Grad, Flock[i]);
	}

	// Copy to Next Latent Point
	NextLatentPoint.Flock.Reset(flockN*Model.q);
	NextLatentPoint.Flock.SetNum(flockN*Model.q);
	for (int i = 0; i < flockN; i++)
	{
		memcpy(NextLatentPoint.Flock.GetData() + i*Model.q, Flock[i].GetData(), sizeof(float)*Model.q);
	}

}

void UGaussianController::BackUpLatentPoint(const FLatentPoint & InputPoint, int steps, const FString & taskname, FLatentPoint & OutputPoint)
{
	int sample = InputPoint.PrevSample;
	auto map = TaskConnectivityMap[taskname].Map;
	for (int s = 0; s < steps; s++)
	{
		sample = *map.FindKey(sample);
	}
	OutputPoint.PrevSample = sample;
	Model.getX(sample, OutputPoint.Coordinates);
}

void UGaussianController::ComputeStartingLatentPoint(const FString & taskname, FLatentPoint & LatentPoint)
{
	// Make sure we have a task, otherwise do nothing.
	if (taskname.Len() == 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("No task name provided"));
		return;
	}

	if (!TaskConnectivityMap.Contains(taskname))
	{
		UE_LOG(LogTemp, Warning, TEXT("No such task %s"), *taskname);
		return;
	}

	int poseindex = TaskConnectivityMap[taskname].Map.CreateIterator().Key();

	LatentPoint.Coordinates.SetNumZeroed(Model.q);
	Model.getX(poseindex, LatentPoint.Coordinates);
	LatentPoint.PrevSample = poseindex;

}



void FGaussianModel::ComputeOutputAtLatentPoint(const TArray<float>& InputPoint, TArray<float>& OutputPoint)
{
	// Initialize our result to the correct dimension, populate with our sample frame
	OutputPoint.Reset(d);
	OutputPoint.SetNum(d);
	//for (int i = 0; i < d; i++) OutputPoint[i] = sample[i];
	//for (int i = 0; i < d; i++) OutputPoint[i] = sample[this->N*i];
	FMemory::Memcpy(OutputPoint.GetData(), sample.GetData(), sizeof(float)*d);
	//return;


	// Use these lines in case we need to disable pose synthesis.
	//return;

	// Ensure we have the correct latent poitn dimension.
	if (InputPoint.Num() != q)
	{
		UE_LOG(LogTemp, Warning, TEXT("Input point recieved of dimension %d not equal to q %d."), InputPoint.Num(), q);
		return;
	}

	// build our model in matlab compatible structures.
	struct0_T mxModel;
	//mxModel.inverseWidth = kern_InverseWidth;
	//mxModel.variance = kern_Variance;
	mxModel.kern_params = emxCreateWrapper_real32_T(kern_params.GetData(), 1, kern_params.Num());
	mxModel.d = reducedD;
	mxModel.N = N;
	mxModel.q = q;
	mxModel.X = emxCreateWrapper_real32_T(X.GetData(), N, q);
	mxModel.scale = emxCreateWrapper_real32_T(scale.GetData(), 1, reducedD);
	mxModel.bias = emxCreateWrapper_real32_T(bias.GetData(), 1, reducedD);
	mxModel.alpha = emxCreateWrapper_real32_T(alpha.GetData(), N, reducedD);
	mxModel.invK_uu = emxCreateWrapper_real32_T(invK_uu.GetData(), N, N);
	emxArray_real32_T* x = emxCreateWrapper_real32_T((float *)(InputPoint).GetData(), 1, InputPoint.Num());
	emxArray_real32_T* y;
	emxInitArray_real32_T(&y, 2);

	// Call the GPLVM
	simpleGpOut(&mxModel, x, y);
	// Copy our data out of the Matlab compatible format.
	int j = 0;
	for (int i = 0; i < d; i++)
	{
		if (dynamicY[i])
		{
			OutputPoint[i] = y->data[j];
			j++;
		}
	}




	// Cleanup matlab allocations.
	emxDestroyArray_real32_T(y);
	emxDestroyArray_real32_T(x);
	emxDestroy_struct0_T(mxModel);

}

void FGaussianModel::ComputeVarGradAtLatentPoint(const TArray<float>& InputPoint, TArray<float>& VarGrad)
{
	// Initialize our result to the correct dimension, populate with our sample frame
	VarGrad.Reset(q);
	VarGrad.SetNum(q);
	//return;


	// Ensure we have the correct latent poitn dimension.
	if (InputPoint.Num() != q)
	{
		UE_LOG(LogTemp, Warning, TEXT("Input point recieved of dimension %d not equal to q %d."), InputPoint.Num(), q);
		return;
	}

	// build our model in matlab compatible structures.
	struct0_T mxModel;
	//mxModel.inverseWidth = kern_InverseWidth;
	//mxModel.variance = kern_Variance;
	mxModel.kern_params = emxCreateWrapper_real32_T(kern_params.GetData(), 1, kern_params.Num());
	mxModel.d = reducedD;
	mxModel.N = N;
	mxModel.q = q;
	mxModel.X = emxCreateWrapper_real32_T(X.GetData(), N, q);
	mxModel.scale = emxCreateWrapper_real32_T(scale.GetData(), 1, reducedD);
	mxModel.bias = emxCreateWrapper_real32_T(bias.GetData(), 1, reducedD);
	mxModel.alpha = emxCreateWrapper_real32_T(alpha.GetData(), N, reducedD);
	mxModel.invK_uu = emxCreateWrapper_real32_T(invK_uu.GetData(), N, N);
	emxArray_real32_T* x = emxCreateWrapper_real32_T((float *)(InputPoint).GetData(), 1, InputPoint.Num());
	emxArray_real32_T* y = emxCreateWrapper_real32_T((float *)(VarGrad).GetData(), 1, InputPoint.Num());
	//emxArray_real32_T* y;
	//emxInitArray_real32_T(&y, 2);
	//emxEnsureCapacity(y, q, (int)sizeof(float));

	// Call the GPLVM
	simpleGpVarGradient(&mxModel, x, y);
	// Copy our data out of the Matlab compatible format.
	memcpy(VarGrad.GetData(), y->data, sizeof(float)*q);

	// Cleanup matlab allocations.
	emxDestroyArray_real32_T(y);
	emxDestroyArray_real32_T(x);
	emxDestroy_struct0_T(mxModel);

}

int FGaussianModel::NearestLatentSample(const TArray<float>& LatentCoordinates)
{
	int nearest_index = -1;
	float nearest_dist = INFINITY;

	for (int i = 0; i < N; i++)
	{
		TArray<float> x;
		getX(i,x);
		float dist = FTArrayMath::DistanceSquared(LatentCoordinates, x);
		if (dist < nearest_dist)
		{
			nearest_dist = dist;
			nearest_index = i;
		}
	}
	return nearest_index;
}



void UGaussianController::SetSkeleton(USkeleton* NewSkeleton)
{
	if (NewSkeleton && NewSkeleton != Skeleton)
	{
		Skeleton = NewSkeleton;
		//SkeletonGuid = NewSkeleton->GetGuid();
	}
}

void UGaussianController::ComputeOptimalLikelihoodLatentPolicy(const FString& taskname, const int32 start_point)
{
	auto address = TaskIndicies[taskname]*Model.extendedN + start_point; // Fine, I'll hash my own damn address
	if (OptimalLikelihoodPolicyMap.Contains(address)) return;

	FGaussianConnectivityMap gcMap;
	gcMap.Map = TaskConnectivityMap[taskname].Map;

	// lets cover the trivial case where we are already in the task.
	if (gcMap.Map.Contains(start_point))
	{
		OptimalLikelihoodPolicyMap.Add(address, gcMap);
		return;
	}

	int steps = FPlatformMath::CeilToInt(OLLNTransitionTimeFrame / SampleRate);
	int V = steps*Model.extendedN;


	int max_to_index = -1;
	float max_ll = -INFINITY;

	TArray<float> lls;
	lls.Init(-INFINITY, V);
	TArray<int32> links;
	links.Init(-1, V);

	// Bellman-Ford
	/*
	{


	for (int round = 0; round < V; round++)
	{
		int round_changes = 0;
		int current = start_point;
		int current_step = -1;
		float current_ll = 0;
		int current_index = (current_step)*Model.extendedN + current;

		// Normally we would for loop through all verticies here, but because we need to handle our special start vertex
		// we instead handle the initial setup and then 'increment' our current and possibly break at the end of the loop.
		while (true)
		{
			// Don't continue to check paths if we are already at our goal.
			if (!gcMap.Map.Contains(current))
			{
				// Handle all the connections for this node.
				for (int to = 0; to < Model.extendedN; to++)
				{
					int to_index = (current_step+1)*Model.extendedN + to;
					float tll = FPlatformMath::Min(TransitionLogLikelihoods[current + to*Model.extendedN], 0.0f);
					float potential_ll = current_ll + tll;
					if (potential_ll > lls[to_index])
					{
						lls[to_index] = potential_ll;
						links[to_index] = current_step*Model.extendedN + current;

						if (gcMap.Map.Contains(to) && potential_ll > max_ll)
						{
							max_ll = potential_ll;
							max_to_index = to_index;
						}

						round_changes += 1;
					}
				}
			}

			if (current_index < 0) current_index = 0;
			else current_index += 1;

			current = (current_index + Model.extendedN) % Model.extendedN;
			current_ll = lls[current_index];
			current_step = FPlatformMath::FloorToInt(current_index * 1.0 / Model.extendedN);
			// no need to scan the final row.
			if (current_step >= steps - 1) break;
		}

		// don't do the full algorithm if the matrix is stable.
		if (round_changes == 0) break;
	}
	}
	*/

	//Dijkstras
	{
		struct LlsCompare
		{
			TArray<float>* lls;
			LlsCompare(TArray<float>* _lls)
			{
				lls = _lls;
			}

			FORCEINLINE bool operator() (const int32& A, const int32& B) const
			{
				return (*lls)[A] > (*lls)[B];
			}
		};
		LlsCompare predicate(&lls);

		TSet<int32> unvisted;
		unvisted.Reserve(V);
		TSet<int32> visited;
		visited.Reserve(V);
		TArray<int32> unvisted_heap;
		unvisted_heap.Reserve(V);
		int unset = V;

		int cnode = start_point;
		int cindex = cnode - Model.extendedN;
		int cstep = -1;
		float ctll = 0;

		while (unvisted.Num() + unset > 0)
		{
			int tstep = cstep + 1;
			if (tstep < steps && !visited.Contains(cindex))
			{
				for (int tnode = 0; tnode < Model.extendedN; tnode++)
				{
					if (tnode == cnode) continue;
					int tindex = tstep*Model.extendedN + tnode;
					if (visited.Contains(tindex)) continue;
					float ttll = ctll + FPlatformMath::Min(TransitionLogLikelihoods[cnode + tnode*Model.extendedN],-0.0001f);
					if (lls[tindex] < ttll)
					{
						lls[tindex] = ttll;
						links[tindex] = cindex;
						if (!unvisted.Contains(tindex))
						{
							unvisted.Add(tindex);
							unvisted_heap.HeapPush(tindex, predicate);
							unset--;
						}
						else
						{
							//int hindex = unvisted_heap.Find(tindex);
							//unvisted_heap.HeapRemoveAt(hindex,predicate,false);
							//unvisted_heap.HeapPush(tindex,predicate);
						}
					}
				}
				//unvisted_heap.VerifyHeap(predicate);
				unvisted_heap.Heapify(predicate);
			}

			visited.Add(cindex);
			unvisted_heap.HeapPop(cindex, predicate);

			cnode = cindex % Model.extendedN;
			cstep = cindex / Model.extendedN;
			ctll = lls[cindex];

			if (gcMap.Map.Contains(cnode))
			{
				max_to_index = cindex;
				max_ll = ctll;
				break;
			}
		}

	}

	// We are done! Backtrack on home.
	int current_step = FPlatformMath::FloorToInt(max_to_index * 1.0 / Model.extendedN);
	int current = (max_to_index + Model.extendedN) % Model.extendedN;
	while (current_step >= 0)
	{
		int current_index = (current_step)*Model.extendedN + current;
		int prev_index = links[current_index];
		int prev = (prev_index + Model.extendedN) % Model.extendedN;
		int prev_step = FPlatformMath::FloorToInt(prev_index * 1.0 / Model.extendedN);
		gcMap.Map.Add(prev, current);
		current_step = prev_step;
		current = prev;
	}
	OptimalLikelihoodPolicyMap.Add(address, gcMap);
	
}

void UGaussianController::ComputeLatentPolicy()
{
	//Find the max transition value
	MaxTransitionLogLikelihood = INT32_MIN;
	for (int i = 0; i < TransitionLogLikelihoods.Num(); i++)
		if (TransitionLogLikelihoods[i] > MaxTransitionLogLikelihood)
			MaxTransitionLogLikelihood = TransitionLogLikelihoods[i];
	// Clear the cached optimal likelihood policies
	OptimalLikelihoodPolicyMap.Reset();

	// Build default connectivity map
	FGaussianConnectivityMap* defaultMap = new FGaussianConnectivityMap();
	for (auto& entry : TaskConnectivityMap)
	{
		defaultMap->Map.Append(entry.Value.Map);
	}


	TMap<FString, bool> looping;
	for (auto& task : SampleData)
	{
		looping.Add(task.TaskName, task.Looping);
	}

	for (auto& entry : TaskConnectivityMap)
	{
		if (entry.Key.Len() == 0) continue;
		PolicyMap.Add(entry.Key);
		ComputeLatentPolicyForTaskByLogLikelihood(entry.Value, *defaultMap, looping[entry.Key], PolicyMap[entry.Key]);
	}

	delete defaultMap;

	for (auto& entry : TaskIndicies)
	{
		TSet<int32> extended_indicies;
		for (int i = 0; i < Model.extendedN; i++)
		{
			auto address = TaskIndicies[entry.Key] * Model.extendedN + i; // Fine, I'll hash my own damn address
			ComputeOptimalLikelihoodLatentPolicy(entry.Key, i);
			for (auto& mapentry : OptimalLikelihoodPolicyMap[address].Map)
			{
				if (mapentry.Key >= Model.N)
				{
					extended_indicies.Add(mapentry.Key);
				}
			}
		}
		for (auto& i : extended_indicies)
		{
			ComputeOptimalLikelihoodLatentPolicy(entry.Key, i);
		}
	}
}

#define STEP_PENALTY 30
void UGaussianController::ComputeLatentPolicyForTaskByLogLikelihood(
	const FGaussianConnectivityMap& given,
	const FGaussianConnectivityMap& defaults,
	const bool& looping,
	FGaussianConnectivityMap& result)
{
	result.Map.Reset();
	result.Map.Append(given.Map);

	TArray<float> totalLL;
	TArray<int> stepCount;
	for (int i = 0; i < Model.extendedN; ++i)
	{
		//totalLL.Add(given.Map.Contains(i) ? TransitionLogLikelihoods[i + given.Map[i] * Model.N] : -INFINITY);
		totalLL.Add(given.Map.Contains(i) ? 0 : -INFINITY);
		stepCount.Add(given.Map.Contains(i) ? 0 : INT32_MAX);
	}

	int numConnected = given.Map.Num();

	// Repeatedly determine the point closest to being integrated into the result, and integrate it.
	while (numConnected < Model.extendedN)
	{
		int bestA = -1;
		int bestB = -1;
		float bestLL = -INFINITY;

		// Find the smallest distance to connect each node to the result
		for (int a = 0; a < Model.extendedN; ++a)
		{
			// Only consider integrating if we aren't in the result yet.
			if (result.Map.Contains(a)) continue;

			for (int b = 0; b < Model.extendedN; ++b)
			{
				// Only consider integrating if the destination is in the result.
				if (!result.Map.Contains(b)) continue;

				float ll = TransitionLogLikelihoods[a + b*Model.extendedN];// +totalLL[b] - STEP_PENALTY*stepCount[b];
				if (ll > bestLL)
				{
					bestA = a;
					bestB = b;
					bestLL = ll;
				}
			}
		}
		totalLL[bestA] = bestLL;
		stepCount[bestA] = stepCount[bestB] + 1;
		result.Map.Add(bestA, bestB);
		++numConnected;
	}
}

#define OFFPATH_SPEED 0.1
void UGaussianController::ComputeLatentPolicyForTaskByDistance(
		const FGaussianConnectivityMap& given, 
		const FGaussianConnectivityMap& defaults, 
		const bool& looping,
		FGaussianConnectivityMap& result)
{
	result.Map.Reset();
	result.Map.Append(given.Map);
	
	//TSet<int32> IncludedIndexes;
	//for (auto& entry : given.Map) IncludedIndexes.Add(entry.Key);

	// Build a distance matrix, favoring going in the same direction as the sandard navigation point.
	TArray<float> distanceMatrix;
	distanceMatrix.SetNumUninitialized(Model.N*Model.N);

	// Build our distance matrix
	for (int a = 0; a < Model.N; ++a)
	{
		TArray<double> pointA;
		Model.getX(a, pointA);


		TArray<double> pointC; // The normal target of this node
		TArray<double> acDelta;
		if (defaults.Map.Contains(a))
		{
			Model.getX(defaults.Map[a], pointC);
			FTArrayMath::Subtract(pointC, pointA, acDelta);
		}

		for (int b = 0; b < Model.N; ++b)
		{
			TArray<double> pointB;
			Model.getX(b, pointB);


			TArray<double> abDelta;
			FTArrayMath::Subtract(pointB, pointA, abDelta);
			
			double dist = FTArrayMath::Length(abDelta);
			if (defaults.Map.Contains(a) && defaults.Map[a] != b) dist /= OFFPATH_SPEED;

			if (defaults.Map.Contains(a))
			{
				if (FTArrayMath::DotProduct(acDelta, abDelta) <= 0.0) dist += 2*FTArrayMath::Length(acDelta); // Penalize if we are 'behind'
			}

			distanceMatrix[a*Model.N + b] = dist;
		}
	}

	TArray<float> totalDistances;
	for (int i = 0; i < Model.N; ++i)
	{
		totalDistances.Add(given.Map.Contains(i) ? 0 : INFINITY);
	}

	int numConnected = given.Map.Num();

	// Repeatedly determine the point closest to being integrated into the result, and integrate it.
	while (numConnected < Model.N)
	{
		int bestA = -1;
		int bestB = -1;
		float bestDist = INFINITY;

		// Find the smallest distance to connect each node to the result
		for (int a = 0; a < Model.N; ++a)
		{
			// Only consider integrating if we aren't in the result yet.
			if (result.Map.Contains(a)) continue;

			for (int b = 0; b < Model.N; ++b)
			{
				float dist = distanceMatrix[a*Model.N + b] + totalDistances[b];
				if (dist < bestDist)
				{
					bestA = a;
					bestB = b;
					bestDist = dist;
				}
			}
		}
		totalDistances[bestA] = bestDist;
		result.Map.Add(bestA, bestB);
		++numConnected;
	}

	UE_LOG(LogTemp, Log, TEXT("Compute Policy!"));

}


// Obligitory defualt constructor.
FGaussianModel::FGaussianModel() :
	q(0),
	N(0),
	d(0)
{
	//UE_LOG(LogTemp, Warning, TEXT("FGaussianModel() was called"));
}


// Constructor
FGaussianModel::FGaussianModel(
	const EGaussianModelKernel _kern_type,
	const TArray<float> _kern_params,
	const int32 _q,
	const int32 _N,
	const int32 _reducedD,
	const int32 _d,
	const TArray<float> _X,
	const TArray<float> _scale,
	const TArray<float> _bias,
	const TArray<float> _alpha,
	const TArray<float> _invK_uu,
	const TArray<bool> _dynamicY,
	const TArray<float> _sample
	//const TArray<int32> _tracks
	) :
	kern_type(_kern_type),
	kern_params(_kern_params),
	q(_q),
	N(_N),
	reducedD(_reducedD),
	d(_d),
	X(_X),
	scale(_scale),
	bias(_bias),
	alpha(_alpha),
	invK_uu(_invK_uu),
	dynamicY(_dynamicY),
	sample(_sample)
{
	extendedX = X;
	extendedN = N;
}

void FGaussianModel::getX(int SampleIndex, TArray<float>& result)
{
	if (extendedN == 0)
	{
		extendedN = N;
		extendedX = X;
	}
	if (SampleIndex < 0) SampleIndex = 0; // Temporary while debugging
	result.Reset();
	for (int i = 0; i < q; i++)
	{
		result.Add(extendedX[SampleIndex + i*extendedN]);
	}
}
void FGaussianModel::getX(int SampleIndex, TArray<double>& result)
{
	if (extendedN == 0)
	{
		extendedN = N;
		extendedX = X;
	}
	if (SampleIndex < 0) SampleIndex = 0; // Temporary while debugging
	result.Reset();
	for (int i = 0; i < q; i++)
	{
		result.Add(extendedX[SampleIndex + i*extendedN]);
	}
}

