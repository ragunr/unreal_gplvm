
#include "ContinuousControllerRuntimePrivatePCH.h"
#include "AnimNode_ComputeLatentPoint.h"
#include "AnimNode_ComputeGaussianPose.h"

FAnimNode_ComputeLatentPoint::FAnimNode_ComputeLatentPoint(void)
	: FAnimNode_Base()
{

}

void FAnimNode_ComputeLatentPoint::Initialize(const FAnimationInitializeContext& Context)
{
	//ComponentPose.Initialize(Context);
}

void FAnimNode_ComputeLatentPoint::Update(const FAnimationUpdateContext& Context)
{
	EvaluateGraphExposedInputs.Execute(Context);
	//ComponentPose.Update(Context);
}

void FAnimNode_ComputeLatentPoint::CacheBones(const FAnimationCacheBonesContext & Context)
{
	//InitializeBoneReferences(Context.AnimInstance->RequiredBones);
	//ComponentPose.CacheBones(Context);
}

void FAnimNode_ComputeLatentPoint::Evaluate(FPoseContext& Output)
{
	PoseLink.Evaluate(Output);
	if (!Controller)
	{
		UE_LOG(LogTemp, Log, TEXT("No Controller provided to ComputeLatentPoint!"));
		return;
	}
	// Cant seem to create output nodes here, so I will use a side effect as a work around.
	//GetLatentPoint(Output.Pose, Controller->BackCalculatedLatentPoint);
	return;
}

void FAnimNode_ComputeLatentPoint::GetLatentPoint(const FCompactPose& Pose, FLatentPoint& Output)
{
	TArray<double> posedata;
	FCompactPose ReferencePose = Pose;
	ReferencePose.ResetToRefPose();
	TArray<FCompactPose> WrappedPose;
	WrappedPose.Add(Pose);
	Controller->SerializePoseData(WrappedPose, ReferencePose, posedata);
	TArray<float> posedata_float;
	FTArrayMath::ToFloats(posedata, posedata_float);
	Controller->ComputeLatentPointAtPose(posedata_float, Output);
	return;
}
