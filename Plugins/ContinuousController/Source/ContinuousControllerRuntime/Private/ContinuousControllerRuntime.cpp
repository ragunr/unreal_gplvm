// Some copyright should be here...

#include "ContinuousControllerRuntimePrivatePCH.h"
#include "gpOut/simpleGp_initialize.h"
#include "gpOut/simpleGp_terminate.h"



#define LOCTEXT_NAMESPACE "FContinuousControllerRuntimeModule"

void FContinuousControllerRuntimeModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	simpleGp_initialize();
	
}

void FContinuousControllerRuntimeModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
	simpleGp_terminate();
	
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FContinuousControllerRuntimeModule, ContinuousControllerRuntime)
