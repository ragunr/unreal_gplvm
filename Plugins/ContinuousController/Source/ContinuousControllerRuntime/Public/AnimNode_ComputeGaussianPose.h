
#pragma once

#include "Engine.h"
#include "Animation/AnimNodeBase.h"
#include "GaussianController.h"
#include "AnimNode_ComputeGaussianPose.generated.h"


USTRUCT()
struct CONTINUOUSCONTROLLERRUNTIME_API FAnimNode_ComputeGaussianPose : public  FAnimNode_Base
{
	GENERATED_USTRUCT_BODY()

	/** Input link(Controller) **/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links, meta=(PinShownByDefault))
	UGaussianController* Controller;

	/** Input link(LatentController) **/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links, meta = (PinShownByDefault))
	mutable FLatentPoint LatentPoint;


public:
	FAnimNode_ComputeGaussianPose();
public:
	// FAnimNode_Base interface
	virtual void Initialize(const FAnimationInitializeContext& Context) override;
	virtual void CacheBones(const FAnimationCacheBonesContext & Context)  override;
	virtual void Update(const FAnimationUpdateContext& Context) override;
	virtual void Evaluate(FPoseContext& Output) override;
};