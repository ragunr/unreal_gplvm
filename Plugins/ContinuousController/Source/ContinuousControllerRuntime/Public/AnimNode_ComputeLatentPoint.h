
#pragma once

#include "Engine.h"
#include "Animation/AnimNodeBase.h"
#include "GaussianController.h"
#include "BonePose.h"
#include "AnimNode_ComputeLatentPoint.generated.h"


USTRUCT()
struct CONTINUOUSCONTROLLERRUNTIME_API FAnimNode_ComputeLatentPoint : public  FAnimNode_Base
{
	GENERATED_USTRUCT_BODY()

	/** Input link(Controller) **/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links, meta=(PinShownByDefault))
	UGaussianController* Controller;

	/** Input link(LatentController) **/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links, meta = (PinShownByDefault))
	mutable FPoseLink PoseLink;


public:
	FAnimNode_ComputeLatentPoint();
public:
	// FAnimNode_Base interface
	virtual void Initialize(const FAnimationInitializeContext& Context) override;
	virtual void CacheBones(const FAnimationCacheBonesContext & Context)  override;
	virtual void Update(const FAnimationUpdateContext& Context) override;
	virtual void Evaluate(FPoseContext& Output) override;

	//UFUNCTION(BlueprintPure, Category = Links)
	void GetLatentPoint(const FCompactPose& Pose, FLatentPoint& Output);
};
