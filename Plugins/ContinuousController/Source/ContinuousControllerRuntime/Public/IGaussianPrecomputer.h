#pragma once

struct FGaussianModel;

struct FGaussianConnectivityMap;

class IGaussianPrecomputer {

public:
	// Responsible for the initial building of the continuous model
	virtual void BuildModel(const TArray<double>& data, 
		int N, 
		const TArray<int32>& ConnectedPairs, 
		FGaussianModel& model, 
		TArray<FGaussianModel>& dynamicsModels, 
		FGaussianModel& reverseModel, 
		TArray<float>& transitionLogLikelihoods) = 0;
};