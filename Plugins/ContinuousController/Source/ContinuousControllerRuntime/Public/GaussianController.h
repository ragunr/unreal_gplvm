// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine.h"
#include "IGaussianPrecomputer.h"
#include "BonePose.h"
#include "Tuple.h"
//#include "Components/SplineComponent.h"
#include "GaussianController.generated.h"


// GPLVM_POSE_SAMPLE_RATE units: Sec / Sample
#define GPLVM_VALUES_PER_BONE 12

USTRUCT(Blueprintable)
struct FLatentPoint
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Coordinates)
	TArray<float> Coordinates;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Coordinates)
	int32 PrevSample = -1;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Coordinates)
	TArray<float> Flock;

};

USTRUCT()
struct FGaussianSample
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, Category = Sample)
	TAssetPtr<UAnimSequence> Animation;
	
	UPROPERTY(EditAnywhere, Category = Sample)
	FString TaskName;

	UPROPERTY(EditAnywhere, Category = Sample)
	bool Looping;

	UPROPERTY(EditAnywhere, Category = Sample)
	FString PreviousTaskName;

	UPROPERTY(EditAnywhere, Category = Sample)
	FString NextTaskName;

	FGaussianSample() : Animation(NULL) {}
	FGaussianSample(TAssetPtr<UAnimSequence> InAnim) : Animation(InAnim) {}


	bool operator==(const FGaussianSample& Other) const
	{
		return (Other.Animation == Animation);
	}
};


UENUM(BlueprintType)
enum class EGaussianModelKernel : uint8
{
	RbfBiasWhite							UMETA(DisplayName = "RBF / Bias / White"),
	Rbf							UMETA(DisplayName = "Radial Basis Function")
};

USTRUCT() 
struct CONTINUOUSCONTROLLERRUNTIME_API FGaussianModel
{
	GENERATED_USTRUCT_BODY()

	// kern // Right now, we are just supporting rbf/bias/white

	UPROPERTY(BlueprintReadOnly, Category = Values, VisibleAnywhere)
	EGaussianModelKernel kern_type;

	UPROPERTY(BlueprintReadOnly, Category = Values, VisibleAnywhere)
	TArray<float> kern_params; 

	UPROPERTY(BlueprintReadOnly, Category=Values, VisibleAnywhere)
	int32 q;  // latent dimensions

	UPROPERTY(BlueprintReadOnly, Category=Values, VisibleAnywhere)
	int32 N;  // Number of samples

	UPROPERTY(BlueprintReadOnly, Category = Values, VisibleAnywhere)
	int32 reducedD;  // reduced input dimensions

	UPROPERTY(BlueprintReadOnly, Category = Values, VisibleAnywhere)
	int32 d;  // input dimensions

	UPROPERTY(BlueprintReadOnly, Category = Values, VisibleAnywhere)
	TArray<float> X; // ?, Nxq dimensionality

	UPROPERTY(BlueprintReadOnly, Category=Values, VisibleAnywhere)
	int32 extendedN;  // Number of samples, including discovered points

	UPROPERTY(BlueprintReadOnly, Category = Values, VisibleAnywhere)
	TArray<float> extendedX; // ?, extendedNxq dimensionality

	UPROPERTY(BlueprintReadOnly, Category = Values, VisibleAnywhere)
	TArray<float> scale; // mean scale, 1xd dimensionality

	UPROPERTY(BlueprintReadOnly, Category = Values, VisibleAnywhere)
	TArray<float> bias; // mean offset, 1xd dimensionality

	UPROPERTY(BlueprintReadOnly, Category = Values, VisibleAnywhere)
	TArray<float> alpha; // precomputed alpha matrix, Nxd dimensionality

	UPROPERTY(BlueprintReadOnly, Category = Values, VisibleAnywhere)
	TArray<float> invK_uu; // precomputed invK_uu matrix, Nxd dimensionality

	UPROPERTY(BlueprintReadOnly, Category = Values, VisibleAnywhere)
	TArray<bool> dynamicY; // precomputed alpha matrix, Nxd dimensionality

	UPROPERTY(BlueprintReadOnly, Category = Values, VisibleAnywhere)
	TArray<float> sample; // sample input, 1xd dimensionality

	//UPROPERTY(Category = Values, VisibleAnywhere)
	//TArray<int32> tracks; // Mapping of tracks to bones

	//const double* alpha; // precomputed alpha matrix, Nxd dimensionality

	void ComputeOutputAtLatentPoint(const TArray<float>& InputPoint, TArray<float>& OutputPoint);
	void ComputeVarGradAtLatentPoint(const TArray<float>& InputPoint, TArray<float>& VarGrad);
	int NearestLatentSample(const TArray<float>& LatentCoordinates);

	FGaussianModel();
	FGaussianModel(
		const EGaussianModelKernel _kern_type,
		const TArray<float> _kern_params,
		const int32 _q,
		const int32 _N,
		const int32 _reducedD,
		const int32 _d,
		const TArray<float> _X,
		const TArray<float> _scale,
		const TArray<float> _bias,
		const TArray<float> _alpha,
		const TArray<float> _invK_uu,
		const TArray<bool> _dynamicY,
		const TArray<float> _sample
		//const TArray<int32> _tracks
		);

	void getX(int SampleIndex, TArray<float>& result);
	void getX(int SampleIndex, TArray<double>& result);
};


/*USTRUCT()
struct CONTINUOUSCONTROLLERRUNTIME_API LatentMovementController
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite, Category = Values, EditAnywhere)
	FLatentPoint position;

	UPROPERTY(BlueprintReadWrite, Category = Values, EditAnywhere)
	TArray<float> velocity;

	UPROPERTY(BlueprintReadWrite, Category = Values, EditAnywhere)
	int TargetSample;
};*/

USTRUCT()
struct CONTINUOUSCONTROLLERRUNTIME_API FGaussianConnectivityMap
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(Category = Map, VisibleAnywhere)
	TMap<int32, int32> Map;

};

UENUM(BlueprintType)
enum class ELatentNavigationPolicy : uint8
{
	/** Use a nearest neighbor method of connecting nodes and then uses linear interpolation to follow those connections. */
	Linear								UMETA(DisplayName = "Linear Interpolation with Nearest Neighbor"),
	/** Navigate via the dynamics model generated durring model creation. */
	Dynamics							UMETA(DisplayName = "Dynamics Model"),
	/** Navigate via the dynamics model generated durring model creation. */
	//Flocking							UMETA(DisplayName = "Flocking based on Linear Interpolation"),
	OptimalLikelihood					UMETA(DisplayName = "Optimal Likelihood")
};

UENUM(BlueprintType)
enum class ESparsificationTechnique : uint8
{
	/** Don't use any sparsification techniques. */
	None								UMETA(DisplayName = "None"),
	/** Cluster based on Euclidian distance. */
	Euclidian							UMETA(DisplayName = "Euclidian Distance"),
	/** Cluster based on Euclidian distance. */
	TierdEuclidian						UMETA(DisplayName = "Teired Euclidian Distance"),
};

/**
 * 
 */
UCLASS(Blueprintable)
class CONTINUOUSCONTROLLERRUNTIME_API UGaussianController : public UObject
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BuildModel)
	float SampleRate = 0.03333;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Variations)
	ELatentNavigationPolicy LatentNavigationPolicy;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Variations)
	bool UseLatentFlocking;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Variations)
	ESparsificationTechnique SparsificationTechnique;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = DynamicsLatentNavigation)
	float DynamicsLNWhite =0.01;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = DynamicsLatentNavigation)
	float DynamicsLNGrad = 0.01;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = FlockingLatentNavigation)
	float FlockingLNMomentum = 0.01;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = FlockingLatentNavigation)
	int FlockingLNBoids = 16;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = FlockingLatentNavigation)
	float FlockingLNCollapse = 0.1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = FlockingLatentNavigation)
	float FlockingLNSpread = 0.1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = FlockingLatentNavigation)
	float FlockingLNGrad = 0.05;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = OptimalLikelihoodLatentNavigation)
	float OLLNTransitionTimeFrame = 2.0;

	/** Model, containing the results of the precomputation */
	UPROPERTY(BlueprintReadOnly, Category = Model, VisibleAnywhere)
	FGaussianModel Model;

	/** Dynamics Model, mapping from latent to latent over time. */
	UPROPERTY(BlueprintReadOnly, Category = Model, VisibleAnywhere)
	FGaussianModel DynamicsModel;

	UPROPERTY(BlueprintReadOnly, Category = Model, VisibleAnywhere)
	TArray<FGaussianModel> DynamicsModels;

	UPROPERTY(BlueprintReadOnly, Category = Model, VisibleAnywhere)
	FGaussianModel ReverseModel;

	UPROPERTY(BlueprintReadOnly, Category = Model, VisibleAnywhere)
	TMap<FString, int32> TaskIndicies;

	UPROPERTY(BlueprintReadOnly, Category = Model, VisibleAnywhere)
	TArray<float> TransitionLogLikelihoods; 

	UPROPERTY(BlueprintReadOnly, Category = Model, VisibleAnywhere)
	float MaxTransitionLogLikelihood;

	UPROPERTY(BlueprintReadWrite, Category = Animation, VisibleAnywhere)
	FString CurrentTask;

	UPROPERTY(Category = Model, VisibleAnywhere)
	TMap<FString, FGaussianConnectivityMap> TaskConnectivityMap;

	UPROPERTY(Category = Model, VisibleAnywhere)
	TArray<int> ConnectedPairs;

	
	UPROPERTY(Category = LatentPolicy, VisibleAnywhere)
	TMap<FString, FGaussianConnectivityMap> PolicyMap;
	
	UPROPERTY(Category = LatentPolicy, VisibleAnywhere)//, Transient)
	TMap<int32, FGaussianConnectivityMap> OptimalLikelihoodPolicyMap;

private:
	/** Pointer to the Skeleton this asset can be played on .	*/
	UPROPERTY(AssetRegistrySearchable, Category = Animation, EditAnywhere)
	class USkeleton* Skeleton;

	/** Skeleton guid. If changes, you need to remap info*/
	FGuid SkeletonGuid;

	/** Meta data that can be saved with the asset
	*
	* You can query by GetMetaData function
	*/
	UPROPERTY(Category = MetaData, instanced, EditAnywhere)
	TArray<class UAnimMetaData*> MetaData;


	void ComputeOptimalLikelihoodLatentPolicy(const FString& taskname, const int32 start_point);
	void ComputeLatentPolicyForTaskByDistance(const FGaussianConnectivityMap& given, const FGaussianConnectivityMap& defaults, const bool& looping, FGaussianConnectivityMap& result);
	void ComputeLatentPolicyForTaskByLogLikelihood(const FGaussianConnectivityMap& given, const FGaussianConnectivityMap& defaults, const bool& looping, FGaussianConnectivityMap& result);

	void ComputeNextLatentPointLinear(const FString& taskname, const FLatentPoint & LatentPoint, float time_delta, FLatentPoint & NextLatentPoint);
	void ComputeNextLatentPointLinear(const FGaussianConnectivityMap& map, const FLatentPoint & LatentPoint, float time_delta, FLatentPoint & NextLatentPoint);
	void ComputeNextLatentPointDynamics(const FString& taskname, const FLatentPoint & LatentPoint, float time_delta, FLatentPoint & NextLatentPoint);
	void ComputeLatentPointFlocking(const FLatentPoint & LatentPoint, float time_delta, FLatentPoint & NextLatentPoint);

	

	void ExtractPoses(TArray<FCompactPose>& PoseSampleData, FCompactPose& ReferencePose);
	void ResetTaskConnectivityMap();
	void PopulateTaskIndicies();
public:
	void SerializePoseData(const TArray<FCompactPose>& PoseSampleData, const FCompactPose& ReferencePose, TArray<double>& data);
	/** Advances the asset player instance
		*
		* @param Instance		AnimationTickRecord Instance - saves data to evaluate
		* @param InstanceOwner	AnimInstance playing this asset
		* @param Context		The tick context (leader/follower, delta time, sync point, etc...)
		*/
	//virtual void TickAssetPlayerInstance(const FAnimTickRecord& Instance, class UAnimInstance* InstanceOwner, FAnimAssetTickContext& Context) const {}
	// this is used in editor only when used for transition getter
	// this doesn't mean max time. In Sequence, this is SequenceLength,
	// but for BlendSpace CurrentTime is normalized [0,1], so this is 1
	//virtual float GetMaxCurrentTime() { return 0.f; }

	void SetSkeleton(USkeleton* NewSkeleton);
	//void ResetSkeleton(USkeleton* NewSkeleton);

	void ComputeLatentPolicy();

#if WITH_EDITORONLY_DATA
	/** Information for thumbnail rendering */
	UPROPERTY(VisibleAnywhere, Instanced, Category = Thumbnail)
	class UThumbnailInfo* ThumbnailInfo;

private:
	/** The default skeletal mesh to use when previewing this asset - this only applies when you open Persona using this asset*/
	//UPROPERTY(duplicatetransient, AssetRegistrySearchable)
	//TAssetPtr<class USkeletalMesh> PreviewSkeletalMesh;
#endif //WITH_EDITORONLY_DATA

public:

	/** Sample animation data **/
	UPROPERTY(EditAnywhere, Category = Samples)
	TArray< FGaussianSample > SampleData;

	UPROPERTY(BlueprintReadOnly)
	FLatentPoint BackCalculatedLatentPoint;

	//UFUNCTION(Exec)
	void ComputeModel(IGaussianPrecomputer* Precomputer);

	class USkeleton* GetSkeleton() const { return Skeleton; }

	UFUNCTION(BlueprintPure, Category=ContinuousController)
	void ComputePoseAtLatentPoint(const FLatentPoint& LatentPoint, TArray<float>& Pose);

	UFUNCTION(BlueprintPure, Category = ContinuousController)
	void ComputeLatentPointAtPose(const TArray<float> Pose, FLatentPoint& LatentPoint);

	//UFUNCTION(BlueprintPure, Category = ContinuousController)
	//void ComputeLatentPointAtPoseLink(const FPoseLink Pose, FLatentPoint& LatentPoint);

	UFUNCTION(BlueprintPure, Category = ContinuousController)
	void ComputeLatentPointGradient(const FLatentPoint & LatentPoint, TArray<float>& Gradient);

	UFUNCTION(BlueprintPure, Category = ContinuousController)
	void ComputeNextLatentPoint(const FString& taskname, const FLatentPoint & LatentPoint, float time_delta, FLatentPoint & NextLatentPoint);

	UFUNCTION(BlueprintPure, Category = ContinuousController)
	void ComputeStartingLatentPoint(const FString& taskname, FLatentPoint & NextLatentPoint);

	UFUNCTION(BlueprintPure, Category = ContinuousController)
	void BackUpLatentPoint(const FLatentPoint& InputPoint, int steps, const FString & taskname, FLatentPoint & OuputPoint);

	//UFUNCTION(BlueprintCallable, Category = ContinuousController)
	//static void rbfKernCompute(double* outK, const double* inX, double inVariance, double inInverseWidth);
	
};
