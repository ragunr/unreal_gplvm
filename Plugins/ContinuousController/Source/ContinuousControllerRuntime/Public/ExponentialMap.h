#pragma once


#include "ExponentialMap.generated.h"

USTRUCT()
struct CONTINUOUSCONTROLLERRUNTIME_API FExponentialMap
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	float theta;

	UPROPERTY()
	FVector w;

	FExponentialMap();

	FExponentialMap(FQuat & Quat);

	FQuat getQuat();

	FVector getVector();
};