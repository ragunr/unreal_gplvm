#pragma once

//#import "GenericPlatformMath.h"

struct CONTINUOUSCONTROLLERRUNTIME_API FTArrayMath
{
	static void Add(const TArray<double>& a, const TArray<double>& b, TArray<double>& result)
	{
		check(a.Num() > 0);
		check(b.Num() > 0);
		check(a.Num() == b.Num());
		result.SetNum(a.Num());
		for (int i = 0; i < a.Num(); i++)
		{
			result[i] = a[i] + b[i];
		}
	}
	static void Add(const TArray<float>& a, const TArray<float>& b, TArray<float>& result)
	{
		check(a.Num() > 0);
		check(b.Num() > 0);
		check(a.Num() == b.Num());
		result.SetNum(a.Num());
		for (int i = 0; i < a.Num(); i++)
		{
			result[i] = a[i] + b[i];
		}
	}

	static void Subtract(const TArray<double>& a, const TArray<double>& b, TArray<double>& result)
	{
		check(a.Num() > 0);
		check(b.Num() > 0);
		check(a.Num() == b.Num());
		result.SetNum(a.Num());
		for (int i = 0; i < a.Num(); i++)
		{
			result[i] = (a[i] - b[i]);
		}
	}

	static void Subtract(const TArray<float>& a, const TArray<float>& b, TArray<float>& result)
	{
		check(a.Num() > 0);
		check(b.Num() > 0);
		check(a.Num() == b.Num());
		result.SetNum(a.Num());
		for (int i = 0; i < a.Num(); i++)
		{
			result[i] = (a[i] - b[i]);
		}
	}

	static void Mult(const TArray<double>& a, double b, TArray<double>& result)
	{
		check(a.Num() > 0);
		result.SetNum(a.Num());
		for (int i = 0; i < a.Num(); i++)
		{
			result[i] = (a[i] * b);
		}
	}

	static void Mult(const TArray<float>& a, double b, TArray<float>& result)
	{
		check(a.Num() > 0);
		result.SetNum(a.Num());
		for (int i = 0; i < a.Num(); i++)
		{
			result[i] = (a[i] * b);
		}
	}

	static void Lerp(const TArray<double>& a, const TArray<double>& b, double alpha, TArray<double>& result)
	{
		check(a.Num() > 0);
		check(b.Num() > 0);
		check(a.Num() == b.Num());
		result.SetNum(a.Num());
		for (int i = 0; i < a.Num(); i++)
		{
			result[i] = (a[i] * (1 - alpha) + b[i] * alpha);
		}
	}

	static void Lerp(const TArray<float>& a, const TArray<float>& b, double alpha, TArray<float>& result)
	{
		check(a.Num() > 0);
		check(b.Num() > 0);
		check(a.Num() == b.Num());
		result.SetNum(a.Num());
		for (int i = 0; i < a.Num(); i++)
		{
			result[i] = (a[i] * (1 - alpha) + b[i] * alpha);
		}
	}


	static double DotProduct(const TArray<double>& a, const TArray<double>& b)
	{
		check(a.Num() > 0);
		check(b.Num() > 0);
		check(a.Num() == b.Num());
		double result = 0;
		for (int i = 0; i < a.Num(); i++)
		{
			result += a[i] * b[i];
		}
		return result;
	}

	static double DistanceSquared(const TArray<double>& a, const TArray<double>& b)
	{
		check(a.Num() > 0);
		check(b.Num() > 0);
		check(a.Num() == b.Num());
		double dist_sqred = 0;
		for (int i = 0; i < a.Num(); i++)
		{
			dist_sqred += (a[i] - b[i])*(a[i] - b[i]);
		}
		return dist_sqred;
	}

	static double DistanceSquared(const TArray<float>& a, const TArray<float>& b)
	{
		check(a.Num() > 0);
		check(b.Num() > 0);
		check(a.Num() == b.Num());
		double dist_sqred = 0;
		for (int i = 0; i < a.Num(); i++)
		{
			dist_sqred += (a[i] - b[i])*(a[i] - b[i]);
		}
		return dist_sqred;
	}

	static double Distance(const TArray<double>& a, const TArray<double>& b)
	{
		check(a.Num() > 0);
		check(b.Num() > 0);
		check(a.Num() == b.Num());
		return FGenericPlatformMath::Sqrt(FTArrayMath::DistanceSquared(a, b));
	}

	static double LengthSquared(const TArray<double>& a)
	{
		check(a.Num() > 0);
		double dist_sqred = 0;
		for (int i = 0; i < a.Num(); i++)
		{
			dist_sqred += (a[i])*(a[i]);
		}
		return dist_sqred;
	}

	static float LengthSquared(const TArray<float>& a)
	{
		check(a.Num() > 0);
		double dist_sqred = 0;
		for (int i = 0; i < a.Num(); i++)
		{
			dist_sqred += (a[i])*(a[i]);
		}
		return dist_sqred;
	}

	static double Length(const TArray<double>& a)
	{
		check(a.Num() > 0);
		return FGenericPlatformMath::Sqrt(FTArrayMath::LengthSquared(a));
	}

	static float Length(const TArray<float>& a)
	{
		check(a.Num() > 0);
		return FGenericPlatformMath::Sqrt(FTArrayMath::LengthSquared(a));
	}

	static void Normalize(const TArray<double>& a, TArray<double>& result)
	{
		check(a.Num() > 0);
		result.SetNum(a.Num());
		float d = FTArrayMath::Length(a);
		for (int i = 0; i < a.Num(); i++)
		{
			result[i] = (a[i] / d);
		}
	}

	static void Normalize(const TArray<float>& a, TArray<float>& result)
	{
		check(a.Num() > 0);
		result.SetNum(a.Num());
		float d = FTArrayMath::Length(a);
		for (int i = 0; i < a.Num(); i++)
		{
			result[i] = (a[i] / d);
		}
	}

	static void ToDoubles(const TArray<float>& a, TArray<double>& result)
	{
		result.SetNum(a.Num());
		for (int i = 0; i < a.Num(); i++)
		{
			result[i] = (a[i]);
		}
	}

	static void ToFloats(const TArray<double>& a, TArray<float>& result)
	{
		result.SetNum(a.Num());
		for (int i = 0; i < a.Num(); i++)
		{
			result[i] = (a[i]);
		}
	}
};