// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ContinuousControllerEditorPrivatePCH.h"
#include "AnimGraphNode_Base.h"
#include "AnimGraphNode_ComputeLatentPoint.generated.h"

/**
 * 
 */
UCLASS(MinimalAPI)
class UAnimGraphNode_ComputeLatentPoint : public UAnimGraphNode_Base
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, Category = Settings)
	FAnimNode_ComputeLatentPoint Node;


public:
	virtual FText GetNodeTitle(ENodeTitleType::Type TitleType) const override;
	virtual FLinearColor GetNodeTitleColor() const override;
	virtual FString GetNodeCategory() const override;

protected:
	virtual FText GetControllerDescription() const;
	
	
};
