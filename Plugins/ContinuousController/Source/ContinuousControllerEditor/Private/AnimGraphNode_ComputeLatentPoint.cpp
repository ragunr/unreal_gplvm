// Fill out your copyright notice in the Description page of Project Settings.

#include "ContinuousControllerEditorPrivatePCH.h"
#include "ContinuousControllerEditor.h"
#include "AnimGraphNode_ComputeLatentPoint.h"


#define LOCTEXT_NAMESPACE "ContinuousController"

//UAnimGraphNode_ComputeLatentPoint::UAnimGraphNode_ComputeLatentPoint(const FObjectInitializer& ObjectInitializer)
//	: Super(ObjectInitializer)
//{
//}

FLinearColor UAnimGraphNode_ComputeLatentPoint::GetNodeTitleColor() const
{
	return FLinearColor(0, 12, 12, 1);
}

FString UAnimGraphNode_ComputeLatentPoint::GetNodeCategory() const
{
	return FString("Continuous Controller");
}
FText UAnimGraphNode_ComputeLatentPoint::GetControllerDescription() const
{
	return LOCTEXT("UAnimGraphNode_ComputeLatentPoint Description", "Compute Latent Point");
}

FText UAnimGraphNode_ComputeLatentPoint::GetNodeTitle(ENodeTitleType::Type TitleType) const
{
	FText Result = GetControllerDescription();
	//Result += (TitleType == ENodeTitleType::ListView) ? TEXT("") : TEXT("\n");
	return Result;
}

#undef LOCTEXT_NAMESPACE 


