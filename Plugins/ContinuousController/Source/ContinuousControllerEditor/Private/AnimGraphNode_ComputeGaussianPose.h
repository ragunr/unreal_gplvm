#pragma once

#include "ContinuousControllerEditorPrivatePCH.h"
#include "AnimGraphNode_ComputeGaussianPose.generated.h"

UCLASS(MinimalAPI)
class UAnimGraphNode_ComputeGaussianPose : public UAnimGraphNode_Base
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(EditAnywhere, Category=Settings)
	FAnimNode_ComputeGaussianPose Node;

	

public: 
	virtual FText GetNodeTitle(ENodeTitleType::Type TitleType) const override;
	virtual FLinearColor GetNodeTitleColor() const override;
	virtual FString GetNodeCategory() const override;

protected:
	virtual FText GetControllerDescription() const;
};