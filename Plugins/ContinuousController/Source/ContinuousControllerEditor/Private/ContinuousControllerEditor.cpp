// Some copyright should be here...

#include "ContinuousControllerEditorPrivatePCH.h"
#include "GaussianControllerComponentDetails.h"

#define LOCTEXT_NAMESPACE "FContinuousControllerEditorModule"

void FContinuousControllerEditorModule::StartupModule()
{

	UE_LOG(LogTemp, Log, TEXT("Continuous Controller Editor Startup!"));
	FPropertyEditorModule& PropertyModule = FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor");

	// Register GaussianController detail customization.
	PropertyModule.RegisterCustomClassLayout(UGaussianController::StaticClass()->GetFName(), FOnGetDetailCustomizationInstance::CreateStatic(&FGaussianControllerComponentDetails::MakeInstance));
	
	PropertyModule.NotifyCustomizationModuleChanged();
	
	
}

void FContinuousControllerEditorModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
	
	
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FContinuousControllerEditorModule, ContinuousControllerEditor)
