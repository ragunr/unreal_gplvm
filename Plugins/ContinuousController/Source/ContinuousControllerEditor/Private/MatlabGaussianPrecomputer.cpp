#include "ContinuousControllerEditorPrivatePCH.h"
#include "MatlabGaussianPrecomputer.h"
//#include <algorithm>

//#define GPLVM_LATENTDIMS 2
//#define GPLVM_ITERATIONS 10

#define LOCTEXT_NAMESPACE "MatlabGaussianPrecomputer"

void FMatlabGaussianPrecomputer::SetIterations(uint32 _iterations)
{
	iterations = _iterations;
}
void FMatlabGaussianPrecomputer::SetLatentDimensions(uint32 _latentDimensions)
{
	latentDimensions = _latentDimensions;
}
void FMatlabGaussianPrecomputer::SetAdoptionPath(const FText& _matPath)
{

	matPath = _matPath;
}
void FMatlabGaussianPrecomputer::SetDiscoverPoses(const bool& _discovery)
{

	discoverPoses = _discovery;
}

void FMatlabGaussianPrecomputer::BuildModel(
	const TArray<double>& data, 
	int N, const TArray<int32>& ConnectedPairs, 
	FGaussianModel& model, 
	TArray<FGaussianModel>& dynamicsModels, 
	FGaussianModel& reverseModel, 
	TArray<float>& transitionLogLikelihoods) {
	
	check(data.Num() % N == 0);
	int d = data.Num() / N;
	// Print dimenisons to log
	//UE_LOG(LogTemp, Warning, TEXT("Using rows : %i, cols : %i"), frames, bones*VALUES_PER_BONE);


	Engine* ep;
	mxArray* result = NULL;
	ep = engOpen("");

	// Set up Y matlab variable
	// @param rows - number of animation frames
	// @param cols - number of bones by components per bone.
	mxArray* Y = mxCreateDoubleMatrix(N, d, mxREAL);
	double* Yarray = mxGetPr(Y);

	memcpy(Yarray, data.GetData(), sizeof(double)*N*d);

	mxArray* connections = mxCreateUninitNumericMatrix(2, ConnectedPairs.Num()/2, mxINT32_CLASS, mxREAL);
	memcpy(mxGetData(connections), ConnectedPairs.GetData(), sizeof(int32)*ConnectedPairs.Num());

	// Declare variables in Matlab
	engPutVariable(ep, "Y", Y);

	engPutVariable(ep, "connections", connections);

	auto mxLatentDimensions = mxCreateDoubleScalar(latentDimensions);
	engPutVariable(ep, "latentDim", mxLatentDimensions);

	auto mxIterations = mxCreateDoubleScalar(iterations);
	engPutVariable(ep, "iterations", mxIterations);

	auto mxDiscoverPoses = mxCreateDoubleScalar(discoverPoses ? 1.0 : 0.0);
	engPutVariable(ep, "discoverPoses", mxDiscoverPoses);

	if (matPath.ToString().Len() > 0)
	{
		auto mxMatPath = mxCreateString(TCHAR_TO_ANSI(*matPath.ToString()));
		engPutVariable(ep, "matPath", mxMatPath);
		engEvalString(ep, "load(matPath);");
		// Ensures we have all of the expected annotations on our model
		engEvalString(ep, "model = gplvmPrepImport(model);");
		mxDestroyArray(mxMatPath);
	}
	else
	{
		//engEvalString(ep, "save recorded/precall.mat");

		// Do it.
		UE_LOG(LogTemp, Warning, TEXT("Build Model!"));
		engEvalString(ep, "model = gplvmPreprocess(Y, latentDim, iterations, 0, connections'+1, discoverPoses);");

		//engEvalString(ep, "save recorded/postcall.mat");
	}

	engEvalString(ep, "save recorded/postcall.mat");

	// copy our model back out.
	model = FGaussianModel(
		(EGaussianModelKernel)(int)ExtractMatlabDouble(ep, "model.kern_type"),
		ExtractMatlabDoubleMatrix(ep, "model.kern_params"), 
		ExtractMatlabInteger(ep, "model.q"),
		ExtractMatlabInteger(ep, "model.N"),
		ExtractMatlabInteger(ep, "model.d"),
		d,
		ExtractMatlabDoubleMatrix(ep, "model.X"),
		ExtractMatlabDoubleMatrix(ep, "model.scale"),
		ExtractMatlabDoubleMatrix(ep, "model.bias"),
		ExtractMatlabDoubleMatrix(ep, "model.alpha"),
		ExtractMatlabDoubleMatrix(ep, "model.invK_uu"),
		ExtractMatlabBooleanMatrix(ep, "model.dynamicY"),
		ExtractMatlabDoubleMatrix(ep, "Y'")
		);

	model.extendedN = ExtractMatlabInteger(ep, "model.expN");
	model.extendedX = ExtractMatlabDoubleMatrix(ep, "model.expX");


	int seriesN = ExtractMatlabDouble(ep, "model.dynamics.numModels");
	dynamicsModels.Reset(seriesN);
	engEvalString(ep, "i = 0;");
	for (int i = 0; i < seriesN; i++)
	{
		engEvalString(ep, "dyn = model.dynamics.comp{i+1};");

		//engEvalString(ep, "save recorded/postcall.mat");
		// copy our model back out.
		dynamicsModels.Add(FGaussianModel(
			(EGaussianModelKernel)(int)ExtractMatlabDouble(ep, "dyn.kern_type"),
			ExtractMatlabDoubleMatrix(ep, "dyn.kern_params"),
			ExtractMatlabInteger(ep, "dyn.q"),
			ExtractMatlabInteger(ep, "dyn.N"),
			ExtractMatlabInteger(ep, "dyn.d"),
			ExtractMatlabInteger(ep, "dyn.d"),
			ExtractMatlabDoubleMatrix(ep, "dyn.X"),
			ExtractMatlabDoubleMatrix(ep, "dyn.scale"),
			ExtractMatlabDoubleMatrix(ep, "dyn.bias"),
			ExtractMatlabDoubleMatrix(ep, "dyn.alpha"),
			ExtractMatlabDoubleMatrix(ep, "dyn.invK_uu"),
			ExtractMatlabBooleanMatrix(ep, "repmat(true,dyn.q,1)"),
			ExtractMatlabDoubleMatrix(ep, "dyn.y'")
			));

		engEvalString(ep, "i = i+1;");

	}
	reverseModel = FGaussianModel(
		(EGaussianModelKernel)(int)ExtractMatlabDouble(ep, "model.reverseModel.kern_type"),
		ExtractMatlabDoubleMatrix(ep, "model.reverseModel.kern_params"), 
		ExtractMatlabInteger(ep, "model.reverseModel.q"),
		ExtractMatlabInteger(ep, "model.reverseModel.N"),
		ExtractMatlabInteger(ep, "model.reverseModel.d"),
		ExtractMatlabInteger(ep, "model.reverseModel.d"),
		ExtractMatlabDoubleMatrix(ep, "model.reverseModel.X"),
		ExtractMatlabDoubleMatrix(ep, "model.reverseModel.scale"),
		ExtractMatlabDoubleMatrix(ep, "model.reverseModel.bias"),
		ExtractMatlabDoubleMatrix(ep, "model.reverseModel.alpha"),
		ExtractMatlabDoubleMatrix(ep, "model.reverseModel.invK_uu"),
		ExtractMatlabBooleanMatrix(ep, "repmat(true,model.reverseModel.d,1)"),
		ExtractMatlabDoubleMatrix(ep, "model.reverseModel.y'")
		);

	transitionLogLikelihoods = ExtractMatlabDoubleMatrix(ep, "model.tll");

	// Cleanup.
	engClose(ep);

	UE_LOG(LogTemp, Warning, TEXT("Model computation finished"));

	mxDestroyArray(Y);
	mxDestroyArray(connections);
	mxDestroyArray(mxLatentDimensions);
	mxDestroyArray(mxIterations);
}

// Grab a variable from matlab based on an expression. Caller must free returned mxArray.
inline mxArray* FMatlabGaussianPrecomputer::ExtractMatlabMxVariable(Engine* ep, const char* expression) {
	engEvalString(ep, "clear ans");
	char prefixed_expression[100];
	strcpy_s(prefixed_expression, "ans = ");
	strcat_s(prefixed_expression, expression);
	strcat_s(prefixed_expression, ";");
	engEvalString(ep, prefixed_expression);
	mxArray* mxVariable = engGetVariable(ep, "ans");
	if (mxVariable == NULL) {
		UE_LOG(LogTemp, Fatal, TEXT("Matlab expresion %s did not contain a value!"), ANSI_TO_TCHAR(expression));
		return NULL;
	}
	return mxVariable;
}

// Get the class name as a TCHAR*
inline TCHAR* FMatlabGaussianPrecomputer::ExtractMatlabClassName(Engine* ep, const char* expression) {
	mxArray* mxVariable = ExtractMatlabMxVariable(ep, expression);
	TCHAR* returnval = ANSI_TO_TCHAR(mxGetClassName(mxVariable));
	mxDestroyArray(mxVariable);
	return returnval;
}

// Get a single double value.
inline float FMatlabGaussianPrecomputer::ExtractMatlabDouble(Engine * ep, const char * expression)
{
	mxArray* mxVariable = ExtractMatlabMxVariable(ep, expression);
	if (!mxIsDouble(mxVariable)) {
		UE_LOG(LogTemp, Fatal, TEXT("Matlab expresion %s did not contain a double!"), expression);
	}
	float returnval = mxGetScalar(mxVariable);
	mxDestroyArray(mxVariable);
	return returnval;
}

// Get a double matrix.
inline TArray<float> FMatlabGaussianPrecomputer::ExtractMatlabDoubleMatrix(Engine * ep, const char * expression)
{
	mxArray* mxVariable = ExtractMatlabMxVariable(ep, expression);
	if (!mxIsDouble(mxVariable)) {
		UE_LOG(LogTemp, Fatal, TEXT("Matlab expresion %s did not contain a double matrix!"), expression);
	}
	int rows = mxGetM(mxVariable);
	int cols = mxGetN(mxVariable);
	TArray<float> result;
	//FMemory::Memcpy(result, (double*)mxGetPr, sizeof(result)); // cant memcopy because we must convert to float
	for (int i = 0; i < rows*cols; i++)
	{
		result.Add((float)((double*)mxGetData(mxVariable))[i]);
	}
	mxDestroyArray(mxVariable);
	return result;
}
// Get a boolean matrix.
inline TArray<bool> FMatlabGaussianPrecomputer::ExtractMatlabBooleanMatrix(Engine * ep, const char * expression)
{
	mxArray* mxVariable = ExtractMatlabMxVariable(ep, expression);
	if (!mxIsLogical(mxVariable)) {
		UE_LOG(LogTemp, Warning, TEXT("Matlab expresion %s did not contain a logical matrix!"), expression);
	}
	int rows = mxGetM(mxVariable);
	int cols = mxGetN(mxVariable);
	TArray<bool> result;
	//FMemory::Memcpy(result, (double*)mxGetPr, sizeof(result)); // cant memcopy because we must convert to float
	for (int i = 0; i < rows*cols; i++)
	{
		result.Add(((bool*)mxGetData(mxVariable))[i]);
	}
	mxDestroyArray(mxVariable);
	return result;
}

// Get an integer. Matlab doesn't really discriminate internally.
inline int FMatlabGaussianPrecomputer::ExtractMatlabInteger(Engine * ep, const char * expression)
{
	mxArray* mxVariable = ExtractMatlabMxVariable(ep, expression);
	if (!mxIsDouble(mxVariable)) {
		UE_LOG(LogTemp, Fatal, TEXT("Matlab expresion %s did not contain an integer!"), expression);
	}
	int returnval = (int)mxGetScalar(mxVariable);
	mxDestroyArray(mxVariable);
	return returnval;
}


#undef LOCTEXT_NAMESPACE