
#include "ContinuousControllerEditorPrivatePCH.h"
#include "AnimGraphNode_ComputeGaussianPose.h"

#define LOCTEXT_NAMESPACE "ContinuousController"

UAnimGraphNode_ComputeGaussianPose::UAnimGraphNode_ComputeGaussianPose(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

FLinearColor UAnimGraphNode_ComputeGaussianPose::GetNodeTitleColor() const
{
	return FLinearColor(0, 12, 12, 1);
}

FString UAnimGraphNode_ComputeGaussianPose::GetNodeCategory() const
{
	return FString("Continuous Controller");
}
FText UAnimGraphNode_ComputeGaussianPose::GetControllerDescription() const
{
	return LOCTEXT("UAnimGraphNode_ComputeGaussianController Description","Compute Gaussian Pose");
}

FText UAnimGraphNode_ComputeGaussianPose::GetNodeTitle(ENodeTitleType::Type TitleType) const
{
	FText Result = GetControllerDescription();
	//Result += (TitleType == ENodeTitleType::ListView) ? TEXT("") : TEXT("\n");
	return Result;
}

#undef LOCTEXT_NAMESPACE 
