// Fill out your copyright notice in the Description page of Project Settings.


#include "ContinuousControllerEditorPrivatePCH.h"
#include "GaussianControllerFactory.h"

#define LOCTEXT_NAMESPACE "GaussianController"

/*------------------------------------------------------------------------------
GaussianControllerFactory.
------------------------------------------------------------------------------*/
UGaussianControllerFactory::UGaussianControllerFactory(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

	SupportedClass = UGaussianController::StaticClass();
	bCreateNew = true;
	bEditAfterNew = true;
}

bool UGaussianControllerFactory::ConfigureProperties()
{
	// Null the parent class so we can check for selection later
	TargetSkeleton = nullptr;

	// Load the content browser module to display an asset picker
	FContentBrowserModule& ContentBrowserModule = FModuleManager::LoadModuleChecked<FContentBrowserModule>("ContentBrowser");

	FAssetPickerConfig AssetPickerConfig;

	/** The asset picker will only show skeletal meshes */
	AssetPickerConfig.Filter.ClassNames.Add(USkeleton::StaticClass()->GetFName());
	AssetPickerConfig.Filter.bRecursiveClasses = true;

	/** The delegate that fires when an asset was selected */
	AssetPickerConfig.OnAssetSelected = FOnAssetSelected::CreateUObject(this, &UGaussianControllerFactory::OnTargetSkeletonSelected);

	/** The default view mode should be a list view */
	AssetPickerConfig.InitialAssetViewType = EAssetViewType::List;

	PickerWindow = SNew(SWindow)
		.Title(LOCTEXT("CreateGaussianControllerOptions", "Pick Skeleton"))
		.ClientSize(FVector2D(500, 600))
		.SupportsMinimize(false).SupportsMaximize(false)
		[
			SNew(SBorder)
			.BorderImage(FEditorStyle::GetBrush("Menu.Background"))
			[
				ContentBrowserModule.Get().CreateAssetPicker(AssetPickerConfig)
			]
		];


	GEditor->EditorAddModalWindow(PickerWindow.ToSharedRef());
	PickerWindow.Reset();

	return TargetSkeleton != nullptr;
}

UObject* UGaussianControllerFactory::FactoryCreateNew(UClass* Class, UObject* InParent, FName Name, EObjectFlags Flags, UObject* Context, FFeedbackContext* Warn)
{
	if (TargetSkeleton)
	{
		UGaussianController * BlendSpace = NewObject<UGaussianController>(InParent, Class, Name, Flags);

		BlendSpace->SetSkeleton(TargetSkeleton);

		return BlendSpace;
	}

	return nullptr;
}

void UGaussianControllerFactory::OnTargetSkeletonSelected(const FAssetData& SelectedAsset)
{
	TargetSkeleton = Cast<USkeleton>(SelectedAsset.GetAsset());
	PickerWindow->RequestDestroyWindow();
}

#undef LOCTEXT_NAMESPACE