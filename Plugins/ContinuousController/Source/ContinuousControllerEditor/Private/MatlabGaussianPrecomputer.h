#pragma once

#include "ContinuousControllerEditorPrivatePCH.h"
#include "matlabengine.h"

class FMatlabGaussianPrecomputer : public IGaussianPrecomputer {

public:
	void BuildModel(
		const TArray<double>& data, 
		int N, 
		const TArray<int32>& ConnectedPairs, 
		FGaussianModel& model, 
		TArray<FGaussianModel>& dynamicsModels, 
		FGaussianModel& reverseModel, 
		TArray<float>& transitionLogLikelihoods);

	void SetIterations(uint32 _iterations);
	void SetLatentDimensions(uint32 _latentDimenions);
	void SetAdoptionPath(const FText& _matPath);
	void SetDiscoverPoses(const bool& _discovery);

private:
	int iterations = 10;
	int latentDimensions = 3;
	bool discoverPoses = false;
	FText matPath;

	static inline mxArray* FMatlabGaussianPrecomputer::ExtractMatlabMxVariable(Engine* ep, const char* expression);
	static inline TCHAR* FMatlabGaussianPrecomputer::ExtractMatlabClassName(Engine* ep, const char* expression);
	static inline float FMatlabGaussianPrecomputer::ExtractMatlabDouble(Engine* ep, const char* expression);
	static inline TArray<float> FMatlabGaussianPrecomputer::ExtractMatlabDoubleMatrix(Engine* ep, const char* expression);
	static inline TArray<bool> FMatlabGaussianPrecomputer::ExtractMatlabBooleanMatrix(Engine* ep, const char* expression);
	static inline int FMatlabGaussianPrecomputer::ExtractMatlabInteger(Engine* ep, const char* expression);

};