// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "ContinuousControllerEditorPrivatePCH.h"
#include "GaussianControllerComponentDetails.h"
#include "MatlabGaussianPrecomputer.h"
#include "PropertyCustomizationHelpers.h"
#include "SNumericEntryBox.h"

#define LOCTEXT_NAMESPACE "GaussianControllerComponentDetails"



TSharedRef<IDetailCustomization> FGaussianControllerComponentDetails::MakeInstance()
{
	UE_LOG(LogTemp, Log, TEXT("Making Sharable"));
	return MakeShareable(new FGaussianControllerComponentDetails);
}

void FGaussianControllerComponentDetails::CustomizeDetails(IDetailLayoutBuilder& DetailLayout)
{
	UE_LOG(LogTemp, Log, TEXT("Customizing!!"));
	const TArray< TWeakObjectPtr<UObject> >& SelectedObjects = DetailLayout.GetDetailsView().GetSelectedObjects();
	for (int32 ObjectIndex = 0; ObjectIndex < SelectedObjects.Num(); ++ObjectIndex)
	{
		const TWeakObjectPtr<UObject>& CurrentObject = SelectedObjects[ObjectIndex];
		if (CurrentObject.IsValid())
		{
			UGaussianController* CurrentCaptureActor = Cast<UGaussianController>(CurrentObject.Get());
			if (CurrentCaptureActor != NULL)
			{
				GaussianController = CurrentCaptureActor;
				break;
			}
		}
	}

	IDetailCategoryBuilder& BuildModelCategory = DetailLayout.EditCategory("Build Model", FText::GetEmpty(), ECategoryPriority::Important);

	//TSharedRef<IPropertyHandle> iterationsHandle = DetailLayout.GetProperty(GET_MEMBER_NAME_CHECKED(FGaussianControllerComponentDetails, iterations));
	//BuildModelCategory.AddProperty(iterationsHandle);

	BuildModelCategory.AddCustomRow(FText::GetEmpty())
		.NameContent()
		[
			SNew(STextBlock)
			.Font(IDetailLayoutBuilder::GetDetailFont())
		.Text(NSLOCTEXT("GaussianControllerDetails", "LatentDimensionsLabel", "Latent Dimensions"))
		]
	.ValueContent()
		[
			SNew(SNumericEntryBox<uint32>)
			.Value(this, &FGaussianControllerComponentDetails::GetLatentDimenisons)
		.AllowSpin(true)
		.MinValue(1).MaxValue(100).MaxSliderValue(20)
		.OnValueChanged(this, &FGaussianControllerComponentDetails::UpdateLatentDimensions)
		];

	BuildModelCategory.AddCustomRow(FText::GetEmpty())
		.NameContent()
		[
			SNew(STextBlock)
			.Font(IDetailLayoutBuilder::GetDetailFont())
		.Text(NSLOCTEXT("GaussianControllerDetails", "IterationsLabel", "Iterations"))
		]
	.ValueContent()
		[
			SNew(SNumericEntryBox<uint32>)
			.Value(this, &FGaussianControllerComponentDetails::GetIterations)
		.AllowSpin(true)
		.MinValue(0).MaxValue(10000).MaxSliderValue(1000)
		.OnValueChanged(this, &FGaussianControllerComponentDetails::UpdateIterations)
		];

	BuildModelCategory.AddCustomRow(FText::GetEmpty())
		.NameContent()
		[
			SNew(STextBlock)
			.Font(IDetailLayoutBuilder::GetDetailFont())
		.Text(NSLOCTEXT("GaussianControllerDetails", "PoseDiscoveryLabel", "Discover Poses"))
		]
	.ValueContent()
		[
			SNew(SCheckBox)
			.IsChecked(discovery)
			.OnCheckStateChanged(this, &FGaussianControllerComponentDetails::UpdatePoseDiscovery)
		];


	// Adopt .Mat file option
	BuildModelCategory.AddCustomRow(FText::GetEmpty())
		.NameContent()
		[
			SNew(STextBlock)
			.Font(IDetailLayoutBuilder::GetDetailFont())
		.Text(NSLOCTEXT("GaussianControllerDetails", "AdoptLabel", "Adopt .Mat file"))
		]
	.ValueContent()
		[
			SNew(SEditableTextBox)
			.Text(matPath)
			.OnTextChanged(this, &FGaussianControllerComponentDetails::UpdateMatPath)
		];


	// Build Button
	BuildModelCategory.AddCustomRow(NSLOCTEXT("GaussianControllerDetails", "UpdateModelMatlab", "MATLAB"))
		.NameContent()
		[
			SNew(STextBlock)
			.Font(IDetailLayoutBuilder::GetDetailFont())
		.Text(NSLOCTEXT("GaussianControllerDetails", "UpdateModelMatlab", "MATLAB"))
		]
	.ValueContent()
		.MaxDesiredWidth(125.f)
		.MinDesiredWidth(125.f)
		[
			SNew(SButton)
			.ContentPadding(2)
		.VAlign(VAlign_Center)
		.HAlign(HAlign_Center)
		.OnClicked(this, &FGaussianControllerComponentDetails::OnMatlabModelUpdate)
		[
			SNew(STextBlock)
			.Font(IDetailLayoutBuilder::GetDetailFont())
		.Text(NSLOCTEXT("GaussianControllerDetails", "ModelUpdateMatlab", "Build in MATLAB"))
		]
		];

	
	// Compute Policy Button
	BuildModelCategory.AddCustomRow(NSLOCTEXT("GaussianControllerDetails", "UpdatePolicyStd", "FindNearestNeighbors"))
		.NameContent()
		[
			SNew(STextBlock)
			.Font(IDetailLayoutBuilder::GetDetailFont())
		.Text(NSLOCTEXT("GaussianControllerDetails", "UpdatePolicyStd", "BuildNearestNeighborWeb"))
		]
	.ValueContent()
		.MaxDesiredWidth(125.f)
		.MinDesiredWidth(125.f)
		[
			SNew(SButton)
			.ContentPadding(2)
		.VAlign(VAlign_Center)
		.HAlign(HAlign_Center)
		.OnClicked(this, &FGaussianControllerComponentDetails::OnUpdatePolicyStdUpdate)
		[
			SNew(STextBlock)
			.Font(IDetailLayoutBuilder::GetDetailFont())
		.Text(NSLOCTEXT("GaussianControllerDetails", "UpdatePolicyStdButton", "Build"))
		]
		];

}



void FGaussianControllerComponentDetails::UpdatePoseDiscovery(ESlateCheckBoxState::Type InState)
{
	discovery = InState == ESlateCheckBoxState::Checked;
}

void FGaussianControllerComponentDetails::UpdateIterations(uint32 new_iterations)
{

	//UE_LOG(LogTemp, Log, TEXT("updated: %i"), new_iterations);
	iterations = new_iterations;
}

TOptional<uint32> FGaussianControllerComponentDetails::GetIterations() const
{
	return iterations;
}

void FGaussianControllerComponentDetails::UpdateLatentDimensions(uint32 _latentDimensions)
{

	//UE_LOG(LogTemp, Log, TEXT("updated: %i"), _latentDimenisons);
	latentDimensions = _latentDimensions;
}

TOptional<uint32> FGaussianControllerComponentDetails::GetLatentDimenisons() const
{
	return latentDimensions;
}
void FGaussianControllerComponentDetails::UpdateMatPath(const FText& _matPath)
{

	matPath.Set(_matPath);
}

TOptional<FText> FGaussianControllerComponentDetails::GetMatPath() const
{
	//if (matPath != NULL) return *matPath;
	//else return TOptional<FString>();
	return matPath.Get();
}

FReply FGaussianControllerComponentDetails::OnMatlabModelUpdate()
{
	if (GaussianController.IsValid())
	{

		UE_LOG(LogTemp, Log, TEXT("iterations: %i, latent dimensions: %i"), iterations, latentDimensions);
		FMatlabGaussianPrecomputer Precomputer;
		Precomputer.SetIterations(iterations);
		Precomputer.SetLatentDimensions(latentDimensions);
		Precomputer.SetDiscoverPoses(discovery);
		Precomputer.SetAdoptionPath(matPath.Get());
		GaussianController->ComputeModel(&Precomputer);
	}

	return FReply::Handled();
}

FReply FGaussianControllerComponentDetails::OnUpdatePolicyStdUpdate()
{
	if (GaussianController.IsValid())
	{

		GaussianController->ComputeLatentPolicy();
	}

	return FReply::Handled();
}

#undef LOCTEXT_NAMESPACE
