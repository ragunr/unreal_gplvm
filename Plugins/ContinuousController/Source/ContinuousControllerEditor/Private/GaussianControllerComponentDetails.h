// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "ContinuousControllerEditorPrivatePCH.h"

class FGaussianControllerComponentDetails : public IDetailCustomization
{
public:
	/** Makes a new instance of this detail layout class for a specific detail view requesting it */ 
	static TSharedRef<IDetailCustomization> MakeInstance();

	uint32 iterations = 10;
	uint32 latentDimensions = 3;
	bool discovery = false;
	TAttribute<FText> matPath;
private:
	/** IDetailCustomization interface */
	virtual void CustomizeDetails(IDetailLayoutBuilder& DetailLayout) override;

	TOptional<uint32> GetIterations() const;
	void UpdateIterations(uint32);
	TOptional<uint32> GetLatentDimenisons() const;
	void UpdateLatentDimensions(uint32);
	void UpdatePoseDiscovery(ESlateCheckBoxState::Type);
	TOptional<FText> GetMatPath() const;
	void UpdateMatPath(const FText&);
	FReply OnMatlabModelUpdate();
	FReply OnUpdatePolicyStdUpdate();
private:
	/** The selected controller */
	TWeakObjectPtr<UGaussianController> GaussianController;
};
