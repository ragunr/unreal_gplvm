// Some copyright should be here...

using UnrealBuildTool;

public class ContinuousControllerEditor : ModuleRules
{
	public ContinuousControllerEditor(TargetInfo Target)
	{
		
		PublicIncludePaths.AddRange(
			new string[] {
				"ContinuousControllerEditor/Public"
				
				// ... add public include paths required here ...
			}
			);
				
		
		PrivateIncludePaths.AddRange(
			new string[] {
				"ContinuousControllerEditor/Private",
				
				// ... add other private include paths required here ...
			}
			);
			
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				
                "Core", "CoreUObject", 
                "UnrealEd", // UnrealEd.h
                "ContinuousControllerRuntime", // ContinuousControllerModule.h
			}
			);
			
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
                "Engine", 
                "PropertyEditor", // PropertyEditing.h
                "ContentBrowser",  // ContentBrowserModule.h
                "InputCore", // InputCoreTypes.h  <- Events.h <- SlateCore.h <- SlateBasics.h <- UnrealEd.h
                "SlateCore", // SlateCore.h <- SlateBasics.h <- UnrealEd.h
                "Slate", // STextBlock.h <- SlateBasics.h <- UnrealEd.h
                "EditorStyle", // EditorStyleSet.h <- EditorStyle.h <- UnrealEd.h
                "AnimGraph", // AnimGraphNode_Base.h
                "BlueprintGraph", // AnimGraphNode_Base.h
                // "PropertyEditor", // Why don't I need this?
                "MatlabEngine", // Experimental
			}
			);
		
		
		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
				
				// ... add any modules that your module loads dynamically here ...
			}
			);
	}
}
